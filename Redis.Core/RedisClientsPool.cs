﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Redis.Core.Interfaces;
using RespService;
using System.Threading;

namespace Redis.Core
{
	public class RedisClientsPool : IRespClientFactory, IDisposable
	{
		private IRedisClientFactory _clientFactory;
		private RespClientPool _clientsPool;
		private RedisEndPoint[] _endPoints;

		public RedisClientsPool(string connectionString)
			: this(connectionString, Redis.Core.RedisClientFactory.Instance)
		{

		}

		public RedisClientsPool(string connectionString, IRedisClientFactory clientFactory)
		{
			if (clientFactory == null)
				throw new ArgumentNullException(nameof(clientFactory));

			_clientFactory = clientFactory;

			var connectionBuilder = new RedisConnectionStringBuilder();
			connectionBuilder.ConnectionString = connectionString;
			this.Database = connectionBuilder.Database;
			_endPoints = connectionBuilder.Servers;
			_clientsPool = new RespClientPool(connectionBuilder.PoolSizeMultiplier, this);
			_clientsPool.Timeout = connectionBuilder.PoolTimeout;
		}

		public void Dispose()
		{
			if (_clientsPool != null)
				_clientsPool.Dispose();
		}

		IRespClientInternal IRespClientFactory.CreateClient()
		{
			RedisEndPoint endPoint = _endPoints.FirstOrDefault();
			if (endPoint == null)
				throw new InvalidOperationException();
			return _clientFactory.CreateRedisClient(endPoint);
		}

		public int Database { get; private set; }

		public int PoolTimeout
		{
			get { return _clientsPool.Timeout; }
			set { _clientsPool.Timeout = value; }
		}

		public int PoolSize
		{
			get { return _clientsPool.PoolSize; }
		}

		public int Available
		{
			get { return _clientsPool.Available; }
		}

		public RespClientPoolPerformance Performance
		{
			get { return _clientsPool.Performance; }
		}

		public virtual async Task<IRedisClient> GetClientAsync()
		{
			IRespClient client = await _clientsPool.GetClientAsync().ConfigureAwait(false);
			var pooledRedis = client as IPooledRedisClient;
			if (pooledRedis != null)
			{
				try
				{
					await pooledRedis.AuthWithDefaultPasswordAsync().ConfigureAwait(false);
					await pooledRedis.ResetTransactionAsync().ConfigureAwait(false);
					await pooledRedis.SelectDefaultDatabaseAsync().ConfigureAwait(false);
				}
				catch (Exception)
				{
					client.Dispose();
					throw;
				}
			}
			return (IRedisClient)client;
		}

		public virtual IRedisClient GetClient()
		{
			IRespClient client = _clientsPool.GetClient();
			var pooledRedis = client as IPooledRedisClient;
			if (pooledRedis != null)
			{
				try
				{
					pooledRedis.AuthWithDefaultPassword();
					pooledRedis.ResetTransaction();
					pooledRedis.SelectDefaultDatabase();
				}
				catch (Exception)
				{
					client.Dispose();
					throw;
				}
			}
			return (IRedisClient)client;
		}
	}
}
