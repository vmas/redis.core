﻿namespace Redis.Core
{
	public enum ObjectSubcommands
	{
		RefCount,
		Encoding,
		IdleTime,
	}
}
