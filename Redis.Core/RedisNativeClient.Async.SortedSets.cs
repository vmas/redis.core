﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<long> ZAddAsync(string key, double score, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return SendQueryExpectNumberAsync("ZADD", key, score.ToString(NumberFormatInfo.InvariantInfo), member);
		}

		public virtual Task<long> ZAddAsync(string key, double defaultScore, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (members == null)
				throw new ArgumentNullException(nameof(members));

			if (members.Length == 0)
				return DefaultLongTask;

			return ZAddAsync(key, ExistenceMode.Default, false, ValidateKeys(members, nameof(members)).Select(item => new KeyValuePair<string, double>(item, defaultScore)));
		}

		public virtual Task<string> ZAddAsync(string key, ExistenceMode mode, bool returnChanged, bool incr, double score, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (member == null)
				throw new ArgumentNullException(nameof(member));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZADD");
			cmdWithArgs.Add(key);
			switch (mode)
			{
				case ExistenceMode.XX:
					cmdWithArgs.Add("XX");
					break;
				case ExistenceMode.NX:
					cmdWithArgs.Add("NX");
					break;
			}

			if (returnChanged)
			{
				cmdWithArgs.Add("CH");
			}

			if (incr)
			{
				cmdWithArgs.Add("INCR");
			}

			cmdWithArgs.Add(score.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(member);
			return SendQueryExpectStringAsync(cmdWithArgs);
		}

		public virtual Task<long> ZAddAsync(string key, ExistenceMode mode, bool returnChanged, IEnumerable<KeyValuePair<string, double>> valuesWithScores)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (valuesWithScores == null)
				throw new ArgumentNullException(nameof(valuesWithScores));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZADD");
			cmdWithArgs.Add(key);

			switch (mode)
			{
				case ExistenceMode.XX:
					cmdWithArgs.Add("XX");
					break;
				case ExistenceMode.NX:
					cmdWithArgs.Add("NX");
					break;
			}

			if (returnChanged)
			{
				cmdWithArgs.Add("CH");
			}

			int countWithoutMembers = cmdWithArgs.Count;
			foreach (KeyValuePair<string, double> kv in valuesWithScores)
			{
				cmdWithArgs.Add(kv.Value.ToString(NumberFormatInfo.InvariantInfo));
				if (kv.Key == null)
					throw new ArgumentOutOfRangeException(nameof(valuesWithScores));
				cmdWithArgs.Add(kv.Key);
			}

			if (cmdWithArgs.Count == countWithoutMembers)
				return DefaultLongTask;

			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> ZCardAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("ZCARD", key);
		}

		public virtual Task<long> ZCountAsync(string key, double min, double max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			string aMin = double.IsInfinity(min) ? "-inf" : min.ToString(NumberFormatInfo.InvariantInfo);
			string aMax = double.IsInfinity(max) ? "+inf" : max.ToString(NumberFormatInfo.InvariantInfo);
			return SendQueryExpectNumberAsync("ZCOUNT", key, aMin, aMax);
		}

		public virtual Task<long> ZCountAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			string aMin, aMax;

			if (double.IsInfinity(min))
				aMin = "-inf";
			else if (excludeMin)
				aMin = "(" + min.ToString(NumberFormatInfo.InvariantInfo);
			else
				aMin = min.ToString(NumberFormatInfo.InvariantInfo);

			if (double.IsInfinity(max))
				aMax = "-inf";
			else if (excludeMax)
				aMax = "(" + max.ToString(NumberFormatInfo.InvariantInfo);
			else
				aMax = max.ToString(NumberFormatInfo.InvariantInfo);

			return SendQueryExpectNumberAsync("ZCOUNT", key, aMin, aMax);
		}

		public virtual async Task<double> ZIncrByAsync(string key, double increment, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return StringToDouble(await SendQueryExpectStringAsync("ZINCRBY", key, increment.ToString(NumberFormatInfo.InvariantInfo), member).ConfigureAwait(false));
		}

		public virtual Task<long> ZInterStoreAsync(string destination, UnionAggregate aggregate, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			if (keys.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			var cmdWithArgs = new List<string>(6 + keys.Length);
			cmdWithArgs.Add("ZINTERSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(keys.Length.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.AddRange(ValidateKeys(keys, nameof(keys)));
			if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			else if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> ZInterStoreAsync(string destination, IEnumerable<string> keys, UnionAggregate aggregate)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return ZInterStoreAsync(destination, aggregate, keys.ToArray());
		}

		public virtual Task<long> ZInterStoreAsync(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keysWithWeights == null)
				throw new ArgumentNullException(nameof(keysWithWeights));

			KeyValuePair<string, double>[] kwdata = keysWithWeights.ToArray();
			if (kwdata.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keysWithWeights));

			var cmdWithArgs = new List<string>(7 + 2 * kwdata.Length);
			cmdWithArgs.Add("ZINTERSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(kwdata.Length.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.AddRange(ValidateKeys(kwdata.Select(kvp => kvp.Key), nameof(keysWithWeights)));
			cmdWithArgs.Add("WEIGHTS");
			cmdWithArgs.AddRange(kwdata.Select(kvp => kvp.Value.ToString(NumberFormatInfo.InvariantInfo)));
			if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			else if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> ZLexCountAsync(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return SendQueryExpectNumberAsync("ZLEXCOUNT", key, min, max);
		}

		public virtual async Task<ICollection<string>> ZRangeAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRangeWithScoresAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo), "WITHSCORES").ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRangeByLexAsync(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZRANGEBYLEX", key, min, max).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRangeByLexAsync(string key, string min, string max, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZRANGEBYLEX", key, min, max,
				"LIMIT", offset.ToString(NumberFormatInfo.InvariantInfo), count.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<long?> ZRankAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			RespData response2 = await SendQueryExpectStatementAsync(MakeCommand("ZRANK", key, member)).ConfigureAwait(false);
			if (IsQueued(response2))
			{
				CreateTransactionTask<RespData, long?>(ParseZRankResult);
				return null;
			}
			return ParseZRankResult(response2);

			long? ParseZRankResult(RespData response)
			{
				if (response.DataType == RespDataType.Integer)
					return ((RespInteger)response).GetInt64();
				if (response.DataType == RespDataType.Bulk)
					return null;
				throw GetUnexpectedReplyException("ZRANK");
			}
		}

		public virtual Task<long> ZRemAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectNumberAsync("ZREM", key, member);
		}

		public virtual Task<long> ZRemAsync(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			return SendQueryExpectNumberAsync(MakeCommand("ZREM", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual Task<long> ZRemAsync(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			string[] membersArg = members.ToArray();
			if (membersArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(members));
			return SendQueryExpectNumberAsync(MakeCommand("ZREM", key, ValidateKeys(membersArg, nameof(members))));
		}

		public virtual Task<long> ZRemRangeByLexAsync(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return SendQueryExpectNumberAsync("ZREMRANGEBYLEX", key, min, max);
		}

		public virtual Task<long> ZRemRangeByRankAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumberAsync("ZREMRANGEBYRANK", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<long> ZRemRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZREMRANGEBYSCORE");
			cmdWithArgs.Add(key);

			if (double.IsInfinity(min))
				cmdWithArgs.Add("-inf");
			else if (excludeMin)
				cmdWithArgs.Add("(" + min.ToString(NumberFormatInfo.InvariantInfo));
			else
				cmdWithArgs.Add(min.ToString(NumberFormatInfo.InvariantInfo));

			if (double.IsInfinity(max))
				cmdWithArgs.Add("+inf");
			else if (excludeMax)
				cmdWithArgs.Add("(" + max.ToString(NumberFormatInfo.InvariantInfo));
			else
				cmdWithArgs.Add(max.ToString(NumberFormatInfo.InvariantInfo));

			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual async Task<ICollection<string>> ZRevRangeAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZREVRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRevRangeWithScoresAsync(string key, long start, long stop)
		{
			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZREVRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo), "WITHSCORES").ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRevRangeByLexAsync(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZREVRANGEBYLEX", key, min, max).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRevRangeByLexAsync(string key, string min, string max, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("ZREVRANGEBYLEX", key, min, max, "LIMIT", offset.ToString(NumberFormatInfo.InvariantInfo), count.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRevRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<string>> ZRevRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSortedSet);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRevRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<ICollection<KeyValuePair<string, double>>> ZRevRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSortedSetWithScores(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<KeyValuePair<string, double>>>(CreateSortedSetWithScores);
			return null;
		}

		public virtual async Task<long?> ZRevRankAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			RespData response2 = await SendQueryExpectStatementAsync(MakeCommand("ZREVRANK", key, member)).ConfigureAwait(false);
			if (IsQueued(response2))
			{
				CreateTransactionTask<RespData, long?>(ParseZRevRankResponse);
				return null;
			}
			return ParseZRevRankResponse(response2);

			long? ParseZRevRankResponse(RespData response)
			{
				if (response.DataType == RespDataType.Integer)
					return ((RespInteger)response).GetInt64();
				if (response.DataType == RespDataType.Bulk)
					return null;
				throw GetUnexpectedReplyException("ZREVRANK");
			}
		}

		public virtual async Task<ScanResult<ICollection<KeyValuePair<string, double>>>> ZScanAsync(string key, long cursor, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				if (count.Value <= 0)
					throw new ArgumentOutOfRangeException(nameof(count));

				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseZScanResult(response2.Value);
			CreateTransactionTask<RespArray, ScanResult<ICollection<KeyValuePair<string, double>>>>(ParseZScanResult);
			return default;

			ScanResult<ICollection<KeyValuePair<string, double>>> ParseZScanResult(RespArray response)
			{
				RespData[] multiData = response.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
					{
						return new ScanResult<ICollection<KeyValuePair<string, double>>>(cursor, CreateSortedSetWithScores((MultiBulk)itemsData));
					}
				}
				throw GetUnexpectedReplyException("ZSCAN");
			}
		}

		public virtual async Task<ScanResult<ICollection<KeyValuePair<string, double>>>> ZScanAsync(string key, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				if (count.Value <= 0)
					throw new ArgumentOutOfRangeException(nameof(count));
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}
			
			var dict = new Dictionary<string, double>();
			while (true)
			{
				RespArray? response = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
				if (!response.HasValue)
					throw GetUnexpectedReplyException("ZSCAN");

				RespData[] multiData = response.Value.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						string aKey = null;
						bool isKey = false;
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							isKey = !isKey;
							if (isKey)
							{
								aKey = bulk.GetString(this.Encoding);
								continue;
							}
							dict[aKey] = StringToDouble(bulk.GetString(this.Encoding));
						}
						if (cursor == 0)
							break;
						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("ZSCAN");
			}
			return new ScanResult<ICollection<KeyValuePair<string, double>>>(0, dict);
		}

		public virtual async Task<double?> ZScoreAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			string score = await SendQueryExpectStringAsync("ZSCORE", key, member).ConfigureAwait(false);
			return score != null ? new double?(StringToDouble(score)) : null;
		}

		public virtual Task<long> ZUnionStoreAsync(string destination, UnionAggregate aggregate, params string[] keys)
		{
			return ZUnionStoreAsync(destination, keys, aggregate);
		}

		public virtual async Task<long> ZUnionStoreAsync(string destination, IEnumerable<string> keys, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));

			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZUNIONSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(null);
			int count = cmdWithArgs.Count;
			cmdWithArgs.AddRange(ValidateKeys(keys, nameof(keys)));
			count = cmdWithArgs.Count - count;
			if (count > 0)
			{
				cmdWithArgs[2] = count.ToString(NumberFormatInfo.InvariantInfo);
			}
			else
			{
				await ExpireAsync(destination, 0).ConfigureAwait(false);
				return 0L;
			}

			if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			else if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			return await SendQueryExpectNumberAsync(cmdWithArgs).ConfigureAwait(false);
		}

		public virtual async Task<long> ZUnionStoreAsync(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keysWithWeights == null)
				throw new ArgumentNullException(nameof(keysWithWeights));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZUNIONSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(null);

			var weights = new List<string>();
			foreach (KeyValuePair<string, double> kvp in keysWithWeights)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keysWithWeights));
				cmdWithArgs.Add(kvp.Key);
				weights.Add(kvp.Value.ToString(NumberFormatInfo.InvariantInfo));
			}
			if (weights.Count == 0)
			{
				await ExpireAsync(destination, 0).ConfigureAwait(false);
				return 0L;
			}
			else
			{
				cmdWithArgs.Add("WEIGHTS");
				cmdWithArgs[2] = weights.Count.ToString(NumberFormatInfo.InvariantInfo);
				cmdWithArgs.AddRange(weights);
			}

			if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			else if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}

			return await SendQueryExpectNumberAsync(cmdWithArgs).ConfigureAwait(false);
		}

	}
}