﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual long HDel(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectNumber("HDEL", key, field);
		}

		public virtual long HDel(string key, string field, params string[] fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			return SendQueryExpectNumber(MakeCommand("HDEL", key, field, fields));
		}

		public virtual long HDel(string key, IEnumerable<string> fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			return SendQueryExpectNumber(MakeCommand("HDEL", key, fields));
		}

		public virtual bool HExists(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectNumber("HEXISTS", key, field) != 0L;
		}

		public virtual string HGet(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectString("HGET", key, field);
		}

		public virtual IEnumerable<KeyValuePair<string, string>> HGetAll(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateDictionary(SendQueryExpectMultiBulk("HGETALL", key));
		}

		public virtual long HIncrBy(string key, string field, long increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			return SendQueryExpectNumber("HINCRBY", key, field, increment.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual double HIncrByFloat(string key, string field, double increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return double.TryParse(
				SendQueryExpectString("HINCRBYFLOAT", key, field, increment.ToString(NumberFormatInfo.InvariantInfo)), 
				NumberStyles.Float,
				NumberFormatInfo.InvariantInfo,
				out double value
				) ? value : double.NaN;
		}

		public virtual IEnumerable<string> HKeys(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateEnumerable(SendQueryExpectMultiBulk("HKEYS", key));
		}

		public virtual long HLen(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("HLEN", key);
		}

		public virtual IEnumerable<string> HMGet(string key, params string[] fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			if (fields.Length == 0)
				return DefaultStringEnumerable;

			return CreateEnumerable(SendQueryExpectMultiBulk(MakeCommand("HMGET", key, fields)));
		}

		public virtual IEnumerable<string> HMGet(string key, IEnumerable<string> fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("HMGET");
			cmdWithArgs.Add(key);
			foreach (string field in fields)
			{
				cmdWithArgs.Add(field);
			}
			if (cmdWithArgs.Count == 2)
				return DefaultStringEnumerable;

			return CreateEnumerable(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual void HMSet(string key, IEnumerable<KeyValuePair<string, string>> fieldsWithValues)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (fieldsWithValues == null)
				throw new ArgumentNullException(nameof(fieldsWithValues));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("HMSET");
			cmdWithArgs.Add(key);

			foreach (var kvp in fieldsWithValues)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(fieldsWithValues));
				cmdWithArgs.Add(kvp.Key);
				cmdWithArgs.Add(kvp.Value);
			}

			if (cmdWithArgs.Count == 2)
				return;

			SendQueryExpectOk(cmdWithArgs);
		}

		public virtual ScanResult<IDictionary<string, string>> HScan(string key, long cursor, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("HSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
			if (multiData.Length == 2)
			{
				RespData cursorData = multiData[0];
				RespData itemsData = multiData[1];

				if (cursorData.DataType == RespDataType.Bulk
					&& itemsData.DataType == RespDataType.Array
					&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
				{
					return new ScanResult<IDictionary<string, string>>(cursor, CreateDictionary((MultiBulk)itemsData));
				}
			}
			throw GetUnexpectedReplyException("HSCAN");
		}

		public virtual ScanResult<IDictionary<string, string>> HScan(string key, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("HSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			var elements = new Dictionary<string, string>();
			while (true)
			{
				RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];

					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						string aKey = null;
						bool isKey = false;
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							isKey = !isKey;
							if (isKey)
							{
								aKey = bulk.GetString(this.Encoding);
							}
							else
							{
								elements[aKey] = bulk.GetString(this.Encoding);
							}
						}
						if (cursor == 0)
							return new ScanResult<IDictionary<string, string>>(0, elements);
						cmdWithArgs[2] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("HSCAN");
			}
		}

		public virtual bool HSet(string key, string field, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return (0L != SendQueryExpectNumber("HSET", key, field, value));
		}

		public virtual bool HSetNX(string key, string field, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return (0L != SendQueryExpectNumber("HSETNX", key, field, value));
		}

		public virtual long HStrLen(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			return SendQueryExpectNumber("HSTRLEN", key, field);
		}

		public virtual IEnumerable<string> HVals(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateEnumerable(SendQueryExpectMultiBulk("HVALS", key));
		}

	}
}
