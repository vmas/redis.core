﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<bool> GeoAddAsync(string key, double longitude, double latitude, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (GeoPosition.IsValidLongitude(longitude))
				throw new ArgumentOutOfRangeException(nameof(longitude));
			if (GeoPosition.IsValidLatitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectBoolAsync("GEOADD", key, longitude.ToString(NumberFormatInfo.InvariantInfo), latitude.ToString(NumberFormatInfo.InvariantInfo), member);
		}

		public virtual Task<bool> GeoAddAsync(string key, GeoObject geo)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (geo.Name != null && !geo.Position.IsValid())
				throw new ArgumentOutOfRangeException(nameof(geo));
			return SendQueryExpectBoolAsync("GEOADD", key, geo.Position.Longitude.ToString(NumberFormatInfo.InvariantInfo), geo.Position.Latitude.ToString(NumberFormatInfo.InvariantInfo), geo.Name);
		}

		public virtual Task<long> GeoAddAsync(string key, IEnumerable<GeoObject> geo)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (geo == null)
				throw new ArgumentNullException(nameof(geo));
			GeoObject[] geodata = geo.ToArray();
			if (geodata.Length == 0)
				return DefaultLongTask;
			var records = new List<string>(geodata.Length * 3);
			foreach (GeoObject item in geodata)
			{
				if (item.Name != null && !item.Position.IsValid())
					throw new ArgumentOutOfRangeException(nameof(geo));
				records.Add(item.Name);
				records.Add(item.Position.Longitude.ToString(NumberFormatInfo.InvariantInfo));
				records.Add(item.Position.Latitude.ToString(NumberFormatInfo.InvariantInfo));
			}
			return SendQueryExpectNumberAsync(MakeCommand("GEOADD", key, records));
		}

		public virtual async Task<string> GeoHashAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			MultiBulk? response = await SendQueryExpectMultiBulkAsync("GEOHASH", key, member).ConfigureAwait(false);
			if (response.HasValue)
				return ParseGeoHashResultAsString(response.Value);
			CreateTransactionTask<MultiBulk, string>(ParseGeoHashResultAsString);
			return null;
		}

		private string ParseGeoHashResultAsString(MultiBulk response)
		{
			return response.First().GetString(this.Encoding);
		}

		public virtual async Task<string[]> GeoHashAsync(string key, string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			if (members.Length == 0)
				return new string[0];
			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("GEOHASH", key, ValidateKeys(members, nameof(members)))).ConfigureAwait(false);
			if (response.HasValue)
				return ParseGeoHashResultAsStringArray(response.Value);
			return null;
		}

		private string[] ParseGeoHashResultAsStringArray(MultiBulk response)
		{
			return response.Select(bulk => bulk.GetString(this.Encoding)).ToArray();
		}

		public virtual async Task<GeoObject?> GeoPosAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			RespArray? response2 = await SendQueryExpectArrayAsync("GEOPOS", key, member).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseGeoPosResult(response2.Value);
			CreateTransactionTask<RespArray, GeoObject?>(ParseGeoPosResult);
			return null;

			GeoObject? ParseGeoPosResult(RespArray response)
			{
				var item = (RespArray)response.First();
				if (item.IsNull())
					return null;
				var geoInfo = item.ToArray();
				if (geoInfo.Length != 2 || geoInfo[0].DataType != RespDataType.Bulk || geoInfo[1].DataType != RespDataType.Bulk)
					throw GetUnexpectedReplyException("GEOPOS");

				return new GeoObject(member, new GeoPosition(
						StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
						StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
					));
			}
		}


		public virtual async Task<IEnumerable<GeoObject>> GeoPosAsync(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			RespArray? response2 = await SendQueryExpectArrayAsync(MakeCommand("GEOPOS", key, member, ValidateKeys(members, nameof(members)))).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseGeoPosResult(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoObject>>(ParseGeoPosResult);
			return null;

			IEnumerable<GeoObject> ParseGeoPosResult(RespArray response)
			{
				int index = -1;
				var locations = new List<GeoObject>();
				foreach (RespArray item in response)
				{
					if (item.IsNull())
					{
						index++;
						continue;
					}

					var geoInfo = item.ToArray();
					if (geoInfo.Length != 2 || geoInfo[0].DataType != RespDataType.Bulk || geoInfo[1].DataType != RespDataType.Bulk)
						throw GetUnexpectedReplyException("GEOPOS");

					locations.Add(new GeoObject(index++ == -1 ? member : members[index],
						new GeoPosition(
							StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
							StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
					)));
				}
				return locations;
			}
		}

		public virtual async Task<IEnumerable<GeoObject>> GeoPosAsync(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			IList<string> membersArg = members as IList<string> ?? members.ToArray();
			if (membersArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(members));

			RespArray? response2 = await SendQueryExpectArrayAsync(MakeCommand("GEOPOS", key, ValidateKeys(members, nameof(members)))).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseGeoPosResult(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoObject>>(ParseGeoPosResult);
			return null;

			IEnumerable<GeoObject> ParseGeoPosResult(RespArray response)
			{
				int index = 0;
				var locations = new List<GeoObject>();
				foreach (RespArray item in response)
				{
					if (item.IsNull())
					{
						index++;
						continue;
					}

					var geoInfo = item.ToArray();
					if (geoInfo.Length != 2)
						throw GetUnexpectedReplyException("GEOPOS");

					locations.Add(new GeoObject(membersArg[index],
						new GeoPosition(
							StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
							StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
					)));
				}
				return locations;
			}
		}

		public virtual Task<double?> GeoDistAsync(string key, string member1, string member2)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member1 == null)
				throw new ArgumentNullException(nameof(member1));
			if (member2 == null)
				throw new ArgumentNullException(nameof(member2));

			return SendQueryExpectNullableDoubleAsync("GEODIST", key, member1, member2);
		}

		public virtual Task<double?> GeoDistAsync(string key, string member1, string member2, GeoUnits unit)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member1 == null)
				throw new ArgumentNullException(nameof(member1));
			if (member2 == null)
				throw new ArgumentNullException(nameof(member2));

			return SendQueryExpectNullableDoubleAsync("GEODIST", key, member1, member2, GeoUnitToString(unit));
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUS_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(13);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual async Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseRadiusResultInternal(response2.Value);
			CreateTransactionTask<RespArray, IEnumerable<GeoData>>(ParseRadiusResultInternal);
			return null;

			IEnumerable<GeoData> ParseRadiusResultInternal(RespArray response)
			{
				return ParseRadiusResult("GEORADIUSBYMEMBER_RO", response, withCoord, withDist, withHash);
			}
		}

		public virtual Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

	}
}
