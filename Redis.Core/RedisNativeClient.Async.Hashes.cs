﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RespService.DataTypes;
using System.Globalization;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<long> HDelAsync(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectNumberAsync("HDEL", key, field);
		}


		public virtual Task<long> HDelAsync(string key, string field, params string[] fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			return SendQueryExpectNumberAsync(MakeCommand("HDEL", key, field, fields));
		}

		public virtual Task<long> HDelAsync(string key, IEnumerable<string> fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			return SendQueryExpectNumberAsync(MakeCommand("HDEL", key, fields));
		}

		public virtual async Task<bool> HExistsAsync(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return (1L == await SendQueryExpectNumberAsync("HEXISTS", key, field).ConfigureAwait(false));

		}

		public virtual Task<string> HGetAsync(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectStringAsync("HGET", key, field);
		}

		public virtual async Task<IEnumerable<KeyValuePair<string, string>>> HGetAllAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response2 = await SendQueryExpectMultiBulkAsync("HGETALL", key).ConfigureAwait(false);
			if (response2.HasValue)
				return CreateDictionary(response2.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<KeyValuePair<string, string>>>(ParseHGetAllResult);
			return null;
		}

		private IEnumerable<KeyValuePair<string, string>> ParseHGetAllResult(MultiBulk response)
		{
			return CreateDictionary(response);
		}

		public virtual Task<long> HIncrByAsync(string key, string field, long increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectNumberAsync("HINCRBY", key, field, increment.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual async Task<double> HIncrByFloatAsync(string key, string field, double increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return await SendQueryExpectNullableDoubleAsync("HINCRBYFLOAT", key, field, increment.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false) ?? 0.0;
		}


		public virtual async Task<IEnumerable<string>> HKeysAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response2 = await SendQueryExpectMultiBulkAsync("HKEYS", key).ConfigureAwait(false);
			if (response2.HasValue)
				return CreateEnumerable(response2.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual Task<long> HLenAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumberAsync("HLEN", key);
		}

		public virtual async Task<IEnumerable<string>> HMGetAsync(string key, params string[] fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			if (fields.Length == 0)
				return DefaultStringEnumerable;

			MultiBulk? response2 = await SendQueryExpectMultiBulkAsync(MakeCommand("HMGET", key, fields)).ConfigureAwait(false);
			if (response2.HasValue)
				return CreateEnumerable(response2.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual async Task<IEnumerable<string>> HMGetAsync(string key, IEnumerable<string> fields)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (fields == null)
				throw new ArgumentNullException(nameof(fields));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("HMGET");
			cmdWithArgs.Add(key);
			foreach (string field in fields)
			{
				cmdWithArgs.Add(field);
			}
			if (cmdWithArgs.Count == 2)
				return DefaultStringEnumerable;

			MultiBulk? response2 = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return CreateEnumerable(response2.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual Task HMSetAsync(string key, IEnumerable<KeyValuePair<string, string>> fieldsWithValues)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (fieldsWithValues == null)
				throw new ArgumentNullException(nameof(fieldsWithValues));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("HMSET");
			cmdWithArgs.Add(key);

			foreach (var kvp in fieldsWithValues)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(fieldsWithValues));
				cmdWithArgs.Add(kvp.Key);
				cmdWithArgs.Add(kvp.Value);
			}

			if (cmdWithArgs.Count == 2)
				return DefaultLongTask;

			return SendQueryExpectOkAsync(cmdWithArgs);
		}

		public virtual async Task<ScanResult<IDictionary<string, string>>> HScanAsync(string key, long cursor, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("HSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseHScanResponse(response2.Value);
			CreateTransactionTask<RespArray, ScanResult<IDictionary<string, string>>>(ParseHScanResponse);
			return default;

			ScanResult<IDictionary<string, string>> ParseHScanResponse(RespArray response)
			{
				RespData[] multiData = response.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];

					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
					{
						return new ScanResult<IDictionary<string, string>>(cursor, CreateDictionary((MultiBulk)itemsData));
					}
				}
				throw GetUnexpectedReplyException("HSCAN");
			}
		}

		public virtual async Task<ScanResult<IDictionary<string, string>>> HScanAsync(string key, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("HSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			var elements = new Dictionary<string, string>();
			while (true)
			{
				RespArray? response = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
				if (!response.HasValue)
					throw GetUnexpectedReplyException("HSCAN");

				RespData[] multiData = response.Value.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];

					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						string aKey = null;
						bool isKey = false;
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							isKey = !isKey;
							if (isKey)
							{
								aKey = bulk.GetString(this.Encoding);
							}
							else
							{
								elements[aKey] = bulk.GetString(this.Encoding);
							}
						}
						if (cursor == 0)
							return new ScanResult<IDictionary<string, string>>(0, elements);
						cmdWithArgs[2] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("HSCAN");
			}
		}

		/// <summary>
		/// Sets field in the hash stored at key to value. If key does not exist, a new key
		/// holding a hash is created. If field already exists in the hash, it is overwritten.
		/// </summary>
		/// <param name="key">The hash key.</param>
		/// <param name="field">The field name.</param>
		/// <param name="value">The field value.</param>
		/// <returns>
		/// True - if field is a new field in the hash and value was set.
		/// False - if field already exists in the hash and the value was updated.
		/// </returns>
		public virtual Task<bool> HSetAsync(string key, string field, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return SendQueryExpectBoolAsync("HSET", key, field, value);
		}

		public virtual Task<bool> HSetNXAsync(string key, string field, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return SendQueryExpectBoolAsync("HSETNX", key, field, value);
		}

		public virtual Task<long> HStrLenAsync(string key, string field)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			return SendQueryExpectNumberAsync("HSTRLEN", key, field);
		}

		public virtual async Task<IEnumerable<string>> HValsAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response2 = await SendQueryExpectMultiBulkAsync("HVALS", key).ConfigureAwait(false);
			if (response2.HasValue)
				return CreateEnumerable(response2.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

	}
}