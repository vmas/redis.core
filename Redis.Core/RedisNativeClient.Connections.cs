﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		private string _password;

		public int Database { get; protected set; }

		public bool IsAuthenticated
		{
			get { return _password != null; }
		}

		public void Auth(string password)
		{
			_password = null;
			SendQueryExpectOk("AUTH", password);
			_password = password;
		}

		public async Task AuthAsync(string password)
		{
			_password = null;
			await SendQueryExpectOkAsync("AUTH", password).ConfigureAwait(false);
			_password = password;
		}

		public string Echo(string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));
			return SendQueryExpectString("ECHO", message);
		}

		public Task<string> EchoAsync(string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));
			return SendQueryExpectStringAsync("ECHO", message);
		}

		public string Ping()
		{
			return SendQueryExpectString("PING");
		}

		public Task<string> PingAsync()
		{
			return SendQueryExpectStringAsync("PING");
		}

		public string Ping(string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));
			return SendQueryExpectString("PING", message);
		}

		public Task<string> PingAsync(string message)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));
			return SendQueryExpectStringAsync("PING", message);
		}

		public void Quit()
		{
			_password = null;
			SendQueryExpectOk("QUIT");
		}

		public Task QuitAsync(string password)
		{
			_password = null;
			return SendQueryExpectOkAsync("QUIT");
		}

		public void Select(int database)
		{
			if (database < 0)
				throw new ArgumentOutOfRangeException(nameof(database));
			SendQueryExpectOk("SELECT", database.ToString(NumberFormatInfo.InvariantInfo));
			this.Database = database;
		}

		public async Task SelectAsync(int database)
		{
			if (database < 0)
				throw new ArgumentOutOfRangeException(nameof(database));
			await SendQueryExpectOkAsync("SELECT", database.ToString(NumberFormatInfo.InvariantInfo));
			this.Database = database;
		}

		public void SwapDB(int a, int b)
		{
			SendQueryExpectOk("SWAPDB", a.ToString(NumberFormatInfo.InvariantInfo), b.ToString(NumberFormatInfo.InvariantInfo));
		}

		public Task SwapDBAsync(int a, int b)
		{
			return SendQueryExpectOkAsync("SWAPDB", a.ToString(NumberFormatInfo.InvariantInfo), b.ToString(NumberFormatInfo.InvariantInfo));
		}

	}
}