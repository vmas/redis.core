namespace Redis.Core
{
	public class RedisClientFactory : IRedisClientFactory
	{
		public static IRedisClientFactory Instance = new RedisClientFactory();

		public RedisClient CreateRedisClient(RedisEndPoint endPoint)
		{
			return new RedisClient(endPoint);
		}
	}
}