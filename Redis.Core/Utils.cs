﻿using System;
using System.Globalization;

namespace Redis.Core
{
	internal static class Utils
	{
		public static string BitOpTypeToString(BitOpType operation)
		{
			switch(operation)
			{
				case BitOpType.And:
					return "AND";
				case BitOpType.Not:
					return "NOT";
				case BitOpType.Or:
					return "OR";
				case BitOpType.Xor:
					return "XOR";
			}
			throw new ArgumentOutOfRangeException(nameof(operation));
		}

		public static double StringToDouble(string s)
		{
			if (double.TryParse(s, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out double value))
				return value;
			if (s == "+inf")
				return double.PositiveInfinity;
			if (s == "-inf")
				return double.NegativeInfinity;
			return double.NaN;
		}

		public static string GeoUnitToString(GeoUnits unit)
		{
			switch (unit)
			{
				case GeoUnits.Meters:
					return "m";
				case GeoUnits.Kilometers:
					return "km";
				case GeoUnits.Miles:
					return "mi";
				case GeoUnits.Feet:
					return "ft";
			}
			throw new ArgumentOutOfRangeException(nameof(unit));
		}

	}
}
