﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		private KeyValuePair<string, string>? ParseBlockingListPopResult(string command, RespArray multibulk)
		{
			if (multibulk.IsNull())
				return null;

			RespData[] multiData = multibulk.ToArray();
			if (multiData.Length == 2)
			{
				RespData keyData = multiData[0];
				RespData valueData = multiData[1];
				if (keyData.DataType == RespDataType.Bulk && valueData.DataType == RespDataType.Bulk)
				{
					return new KeyValuePair<string, string>(((RespBulk)keyData).GetString(this.Encoding), ((RespBulk)valueData).GetString(this.Encoding));
				}
			}
			throw GetUnexpectedReplyException(command);
		}

		public virtual KeyValuePair<string, string>? BLPop(string key, long timeout)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			return ParseBlockingListPopResult("BLPOP", SendQueryExpectArray("BLPOP", key, timeout.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual KeyValuePair<string, string>? BLPop(long timeout, string key, params string[] keys)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return ParseBlockingListPopResult("BLPOP", SendQueryExpectArray(MakeCommand("BLPOP", key, ValidateKeys(keys, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))));
		}

		public virtual KeyValuePair<string, string>? BLPop(IEnumerable<string> keys, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			IList<string> keysArg = keys as IList<string> ?? keys.ToArray();
			if (keysArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return ParseBlockingListPopResult("BLPOP", SendQueryExpectArray(MakeCommand("BLPOP", ValidateKeys(keysArg, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))));
		}

		public virtual KeyValuePair<string, string>? BRPop(string key, long timeout)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			return ParseBlockingListPopResult("BRPOP", SendQueryExpectArray("BRPOP", key, timeout.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual KeyValuePair<string, string>? BRPop(long timeout, string key, params string[] keys)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return ParseBlockingListPopResult("BRPOP", SendQueryExpectArray(MakeCommand("BRPOP", key, ValidateKeys(keys, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))));
		}

		public virtual KeyValuePair<string, string>? BRPop(IEnumerable<string> keys, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			IList<string> keysArg = keys as IList<string> ?? keys.ToArray();
			if (keysArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return ParseBlockingListPopResult("BLPOP", SendQueryExpectArray(MakeCommand("BLPOP", ValidateKeys(keysArg, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))));
		}

		public virtual string BRPopLPush(string source, string destination, long timeout)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			return SendQueryExpectString("BRPOPLPUSH", source, destination, timeout.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual string LIndex(string key, long index)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("LINDEX", key, index.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual long LInsert(string key, bool before, string pivot, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (pivot == null)
				throw new ArgumentNullException(nameof(pivot));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("LINSERT", key, before ? "BEFORE" : "AFTER", pivot.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual long LLen(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("LLEN", key);
		}

		public virtual string LPop(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("LPOP", key);
		}

		public virtual long LPush(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("LPUSH", key, value);
		}

		public virtual long LPush(string key, string value, params string[] values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			return SendQueryExpectNumber(MakeCommand("LPUSH", key, value, ValidateKeys(values, nameof(values))));
		}

		public virtual long LPush(string key, IEnumerable<string> values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			IList<string> valuesArg = values as IList<string> ?? values.ToArray();
			if (valuesArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(values));
			return SendQueryExpectNumber(MakeCommand("LPUSH", key, ValidateKeys(valuesArg, nameof(values))));
		}

		public virtual long LPushX(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("LPUSHX", key, value);
		}

		public virtual IList<string> LRange(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return new List<string>(CreateEnumerable(SendQueryExpectMultiBulk("LRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo))));
		}

		public virtual long LRem(string key, long count, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("LREM", count.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual void LSet(string key, long index, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			SendQueryExpectOk("LSET", key, index.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual void LTrim(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			SendQueryExpectOk("LTRIM", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual string RPop(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("RPOP", key);
		}

		public virtual string RPopLPush(string source, string destination)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			return SendQueryExpectString("RPOPLPUSH", source, destination);
		}

		public virtual long RPush(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("RPUSH", key, value);
		}

		public virtual long RPush(string key, string value, params string[] values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			return SendQueryExpectNumber(MakeCommand("RPUSH", key, value, ValidateKeys(values, nameof(values))));
		}

		public virtual long RPush(string key, IEnumerable<string> values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			IList<string> valuesArg = values as IList<string> ?? values.ToArray();
			if (valuesArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(values));
			return SendQueryExpectNumber(MakeCommand("RPUSH", key, ValidateKeys(valuesArg, nameof(values))));
		}

		public virtual long RPushX(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("RPUSHX", key, value);
		}
	}
}
