﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using RespService.DataTypes;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual async Task<KeyValuePair<string, string>?> BLPopAsync(string key, long timeout)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));

			RespArray? response = await SendQueryExpectArrayAsync("BLPOP", key, timeout.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BLPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BLPOP", a));
			return null;
		}

		public virtual async Task<KeyValuePair<string, string>?> BLPopAsync(long timeout, string key, params string[] keys)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			RespArray? response = await SendQueryExpectArrayAsync(MakeCommand("BLPOP", key, ValidateKeys(keys, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BLPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BLPOP", a));
			return null;
		}

		public virtual async Task<KeyValuePair<string, string>?> BLPopAsync(IEnumerable<string> keys, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			IList<string> keysArg = keys as IList<string> ?? keys.ToArray();
			if (keysArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));

			RespArray? response = await SendQueryExpectArrayAsync(MakeCommand("BLPOP", ValidateKeys(keysArg, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BLPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BLPOP", a));
			return null;
		}

		public virtual async Task<KeyValuePair<string, string>?> BRPopAsync(string key, long timeout)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));

			RespArray? response = await SendQueryExpectArrayAsync("BRPOP", key, timeout.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BRPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BRPOP", a));
			return null;
		}

		public virtual async Task<KeyValuePair<string, string>?> BRPopAsync(long timeout, string key, params string[] keys)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			RespArray? response = await SendQueryExpectArrayAsync(MakeCommand("BRPOP", key, ValidateKeys(keys, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BRPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BRPOP", a));
			return null;
		}

		public virtual async Task<KeyValuePair<string, string>?> BRPopAsync(IEnumerable<string> keys, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			IList<string> keysArg = keys as IList<string> ?? keys.ToArray();
			if (keysArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));

			RespArray? response = await SendQueryExpectArrayAsync(MakeCommand("BRPOP", ValidateKeys(keysArg, nameof(keys)), timeout.ToString(NumberFormatInfo.InvariantInfo))).ConfigureAwait(false);
			if (response.HasValue)
				return ParseBlockingListPopResult("BRPOP", response.Value);
			CreateTransactionTask<RespArray, KeyValuePair<string, string>?>(a => ParseBlockingListPopResult("BRPOP", a));
			return null;
		}

		public virtual Task<string> BRPopLPushAsync(string source, string destination, long timeout)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));
			return SendQueryExpectStringAsync("BRPOPLPUSH", source, destination, timeout.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<string> LIndexAsync(string key, long index)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("LINDEX", key, index.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<long> LInsertAsync(string key, bool before, string pivot, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (pivot == null)
				throw new ArgumentNullException(nameof(pivot));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("LINSERT", key, before ? "BEFORE" : "AFTER", pivot.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task<long> LLenAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("LLEN", key);
		}

		public virtual Task<string> LPopAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("LPOP", key);
		}

		public virtual Task<long> LPushAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("LPUSH", key, value);
		}

		public virtual Task<long> LPushAsync(string key, string value, params string[] values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			return SendQueryExpectNumberAsync(MakeCommand("LPUSH", key, value, ValidateKeys(values, nameof(values))));
		}

		public virtual Task<long> LPushAsync(string key, IEnumerable<string> values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			IList<string> valuesArg = values as IList<string> ?? values.ToArray();
			if (valuesArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(values));
			return SendQueryExpectNumberAsync(MakeCommand("LPUSH", key, ValidateKeys(valuesArg, nameof(values))));
		}

		public virtual Task<long> LPushXAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("LPUSHX", key, value);
		}

		public virtual async Task<IList<string>> LRangeAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			MultiBulk? response = await SendQueryExpectMultiBulkAsync("LRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return new List<string>(CreateEnumerable(response.Value));
			CreateTransactionTask<MultiBulk, IList<string>>(a => new List<string>(CreateEnumerable(a)));
			return null;
		}

		public virtual Task<long> LRemAsync(string key, long count, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("LREM", count.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task LSetAsync(string key, long index, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectOkAsync("LSET", key, index.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task LTrimAsync(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectOkAsync("LTRIM", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<string> RPopAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("RPOP", key);
		}

		public virtual Task<string> RPopLPushAsync(string source, string destination)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			return SendQueryExpectStringAsync("RPOPLPUSH", source, destination);
		}

		public virtual Task<long> RPushAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("RPUSH", key, value);
		}

		public virtual Task<long> RPushAsync(string key, string value, params string[] values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			return SendQueryExpectNumberAsync(MakeCommand("RPUSH", key, value, ValidateKeys(values, nameof(values))));
		}

		public virtual Task<long> RPushAsync(string key, IEnumerable<string> values)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			IList<string> valuesArg = values as IList<string> ?? values.ToArray();
			if (valuesArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(values));
			return SendQueryExpectNumberAsync(MakeCommand("RPUSH", key, ValidateKeys(valuesArg, nameof(values))));
		}

		public virtual Task<long> RPushXAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("RPUSHX", key, value);
		}

	}
}
