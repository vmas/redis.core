﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public async Task DiscardAsync()
		{
			await SendQueryExpectOkAsync("DISCARD").ConfigureAwait(false);
			CleanTransactionTasks();
		}

		public async Task<object[]> ExecAsync()
		{
			RespData response = await SendQueryExpectStatementAsync(new string[1] { "EXEC" }).ConfigureAwait(false);
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.Array)
			{
				RespArray array = ((RespArray)response);
				Queue<ITransactionTask> tasks = Interlocked.Exchange(ref _transactionTasks, null);
				if (array.GetLength() != tasks.Count)
					throw GetUnexpectedReplyException("EXEC");

				int index = 0;
				var result = new object[tasks.Count];
				foreach (RespData item in array)
				{
					result[index++] = tasks.Dequeue().Execute(item);
				}
				return result;
			}
			else if (dataType == RespDataType.String)
			{
				if (((RespString)response).GetString(this.Encoding) == null)
					return null;
			}
			throw GetUnexpectedReplyException("EXEC");
		}

		public async Task MultiAsync()
		{
			if (_transactionTasks != null)
				throw new InvalidOperationException();
			await SendQueryExpectOkAsync("MULTI").ConfigureAwait(false);
			_transactionTasks = new Queue<ITransactionTask>();
		}

	}
}
