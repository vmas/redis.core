﻿using System;

namespace Redis.Core
{
	public struct GeoObject
	{
		private string _name;
		private GeoPosition _location;

		public GeoObject(string name)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));
			_name = name;
			_location = default(GeoPosition);
		}

		public GeoObject(string name, GeoPosition location)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));
			if (!location.IsValid())
				throw new ArgumentNullException(nameof(location));
			_name = name;
			_location = location;
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public GeoPosition Position
		{
			get { return _location; }
			set { _location = value; }
		}

		public override string ToString()
		{
			return _name ?? "";
		}
	}
}
