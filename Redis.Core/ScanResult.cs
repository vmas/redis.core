﻿namespace Redis.Core
{
	public struct ScanResult<T>
	{
		private readonly long _cursor;
		private readonly T _elements;

		public ScanResult(long cursor, T elements)
		{
			_cursor = cursor;
			_elements = elements;
		}

		public long Cursor
		{
			get { return _cursor; }
		}

		public T Elements
		{
			get { return _elements; }
		}
	}
}
