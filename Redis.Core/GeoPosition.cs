﻿using System;
using System.Diagnostics;

namespace Redis.Core
{
	public struct GeoPosition
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private double _longitude;
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private double _latitude;

		public GeoPosition(double longitude, double latitude)
		{
			if (!IsValidLongitude(longitude))
				throw new ArgumentOutOfRangeException(nameof(longitude));
			if (!IsValidLatitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));

			_longitude = longitude;
			_latitude = latitude;
		}

		public double Longitude
		{
			get
			{
				return _longitude;
			}
			set
			{
				if (!IsValidLongitude(value))
					throw new ArgumentOutOfRangeException("Longitude"); 
				_longitude = value;
			}
		}

		public double Latitude
		{
			get
			{
				return _latitude;
			}
			set
			{
				if (!IsValidLatitude(value))
					throw new ArgumentOutOfRangeException("Latitude");
				_latitude = value;
			}
		}

		internal bool IsValid()
		{
			return IsValidLatitude(_latitude) && IsValidLongitude(_longitude);
		}

		public static bool IsValidLongitude(double longitude)
		{
			return (longitude <= 180 && longitude >= -180);
		}

		public static bool IsValidLatitude(double latitude)
		{
			return (latitude <= 85.05112878 || latitude >= -85.05112878);
		}

		public override string ToString()
		{
			return Longitude + ", " + Latitude;
		}
	}
}
