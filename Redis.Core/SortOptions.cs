﻿namespace Redis.Core
{
	public sealed class SortOptions
	{
		public string SortPattern { get; set; }
		public OffsettedRange? Limit { get; set; }
		public string[] GetPatterns { get; set; }
		public bool Alpha { get; set; }
		public bool Desc { get; set; }
		public string StoreAtKey { get; set; }
	}
}
