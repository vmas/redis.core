﻿using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class Int64TransactionTask : ITransactionTask
	{
		private readonly string _commandName;

		public Int64TransactionTask(string commandName)
		{
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64();
			throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
