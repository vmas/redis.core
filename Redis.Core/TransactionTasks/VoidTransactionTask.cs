﻿using System;
using System.Text;
using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class VoidTransactionTask : ITransactionTask
	{
		private readonly string _commandName;
		private readonly Encoding _encoding;

		public VoidTransactionTask(string commandName, Encoding encoding)
		{
			_encoding = encoding;
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			if (response.DataType == RespDataType.String)
				return "OK".Equals(((RespString)response).GetString(_encoding), StringComparison.OrdinalIgnoreCase);
			throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
