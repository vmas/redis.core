﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class GenericTransactionTask<TData, TResult> : ITransactionTask
	{
		private readonly string _commandName;
		private readonly Func<TData, TResult> _parse;

		public GenericTransactionTask(string commandName, Func<TData, TResult> parse)
		{
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			return _parse((TData)(object)response);
			//throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
