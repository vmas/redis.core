﻿using System.Text;
using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class StringTransactionTask : ITransactionTask
	{
		private readonly string _commandName;
		private readonly Encoding _encoding;

		public StringTransactionTask(string commandName, Encoding encoding)
		{
			_encoding = encoding;
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			if (response.DataType == RespDataType.String)
				return ((RespString)response).GetString(_encoding);
			if (response.DataType == RespDataType.Bulk)
				return ((RespBulk)response).GetString(_encoding);
			throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
