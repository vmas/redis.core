﻿using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class BooleanTransactionTask : ITransactionTask
	{
		private readonly string _commandName;

		public BooleanTransactionTask(string commandName)
		{
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64() != 0L;
			throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
