﻿using System.Text;
using Redis.Core.Interfaces;
using RespService.DataTypes;

namespace Redis.Core.TransactionTasks
{
	internal sealed class NullableDoubleTransactionTask : ITransactionTask
	{
		private readonly string _commandName;
		private readonly Encoding _encoding;

		public NullableDoubleTransactionTask(string commandName, Encoding encoding)
		{
			_encoding = encoding;
			_commandName = commandName;
		}

		public object Execute(RespData response)
		{
			if (response.DataType == RespDataType.String)
				return ((RespString)response).GetString(_encoding);
			if (response.DataType == RespDataType.Bulk)
			{
				string s = ((RespBulk)response).GetString(_encoding);
				return s is null ? default(double?) : new double?(Utils.StringToDouble(s));
			}
			throw RedisNativeClient.GetUnexpectedReplyException(_commandName);
		}
	}
}
