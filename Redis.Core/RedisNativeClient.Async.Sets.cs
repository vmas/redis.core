﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual async Task<bool> SAddAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return (1L == await SendQueryExpectNumberAsync("SADD", key, member).ConfigureAwait(false));
		}

		public virtual Task<long> SAddAsync(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			return SendQueryExpectNumberAsync(MakeCommand("SADD", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual Task<long> SAddAsync(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("SADD");
			cmdWithArgs.Add(key);
			cmdWithArgs.AddRange(ValidateKeys(members, nameof(members)));
			if (cmdWithArgs.Count == 2)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(cmdWithArgs);
		}

		public virtual Task<long> SCardAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("SCARD", key);
		}

		public virtual async Task<ICollection<string>> SDiffAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SDIFF", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk,ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SDiffAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SDIFF", key, ValidateKeys(keys, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SDiffAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SDIFF", ValidateKeys(keysArg, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<long> SDiffStoreAsync(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("SDIFFSTORE", destination, key);
		}

		public virtual Task<long> SDiffStoreAsync(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SDIFFSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task<long> SDiffStoreAsync(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SDIFFSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual async Task<ICollection<string>> SInterAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SINTER", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SInterAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SINTER", key, ValidateKeys(keys, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SInterAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SINTER", ValidateKeys(keysArg, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<long> SInterStoreAsync(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("SINTERSTORE", destination, key);
		}

		public virtual Task<long> SInterStoreAsync(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SINTERSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task<long> SInterStoreAsync(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SINTERSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual Task<bool> SIsMemberAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return SendQueryExpectBoolAsync("SISMEMBER", key, member);
		}

		public virtual async Task<ICollection<string>> SMembersAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SMEMBERS", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<bool> SMoveAsync(string source, string destination, string member)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectBoolAsync("SMOVE", source, destination, member);
		}

		public virtual Task<string> SPopAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("SPOP", key);
		}

		public virtual async Task<ICollection<string>> SPopAsync(string key, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SPOP", key, count.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<string> SRandMemberAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("SRANDMEMBER", key);
		}

		public virtual async Task<ICollection<string>> SRandMemberAsync(string key, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SRANDMEMBER", key, count.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<long> SRemAsync(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectNumberAsync("SREM", key, member);
		}

		public virtual Task<long> SRemAsync(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			return SendQueryExpectNumberAsync(MakeCommand("SREM", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual Task<long> SRemAsync(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			string[] membersArg = members.ToArray();
			if (membersArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(members));
			return SendQueryExpectNumberAsync(MakeCommand("SREM", key, ValidateKeys(membersArg, nameof(members))));
		}

		public virtual async Task<ScanResult<ICollection<string>>> SScanAsync(string key, long cursor, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("SSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseSScanResult(response2.Value);
			CreateTransactionTask<RespArray, ScanResult<ICollection<string>>>(ParseSScanResult);
			return default;

			ScanResult<ICollection<string>> ParseSScanResult(RespArray response)
			{
				RespData[] multiData = response.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
					{
						return new ScanResult<ICollection<string>>(cursor, CreateSet((MultiBulk)itemsData));
					}
				}
				throw GetUnexpectedReplyException("SSCAN");
			}
		}

		public virtual async Task<ScanResult<ICollection<string>>> SScanAsync(string key, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("SSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			var result = new HashSet<string>();
			while (true)
			{
				RespArray? response = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
				if (!response.HasValue)
					throw GetUnexpectedReplyException("SSCAN");

				RespData[] multiData = response.Value.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							result.Add(bulk.GetString(this.Encoding));
						}

						if (cursor == 0)
							return new ScanResult<ICollection<string>>(0, result);

						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}

				}
				throw GetUnexpectedReplyException("SSCAN");
			}
		}

		public virtual async Task<ICollection<string>> SUnionAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SUNION", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SUnionAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SUNION", key, keys)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual async Task<ICollection<string>> SUnionAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return new HashSet<string>();

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("SUNION", keysArg)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateSet(response.Value);
			CreateTransactionTask<MultiBulk, ICollection<string>>(CreateSet);
			return null;
		}

		public virtual Task<long> SUnionStoreAsync(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("SUNIONSTORE", destination, key);
		}

		public virtual Task<long> SUnionStoreAsync(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SUNIONSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task<long> SUnionStoreAsync(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("SUNIONSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

	}
}