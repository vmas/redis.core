﻿namespace Redis.Core
{
	public struct GeoData
	{

		public GeoData(string name)
		{
			this.Name = name;
			this.Location = null;
			this.Distance = null;
			this.Hash = null;
		}

		public string Name { get; set; }

		public double? Distance { get; set; }

		public long? Hash { get; set; }

		public GeoPosition? Location { get; set; }

		public override string ToString()
		{
			return this.Name ?? "";
		}
	}
}
