﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual bool Del(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("DEL", key) != 0L;
		}

		public virtual long Del(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("DEL", ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual long Del(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("DEL", key, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual string Dump(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("DUMP", key);
		}

		public virtual long Exists(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("EXISTS", keysArg));
		}

		public virtual bool Exists(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber(MakeCommand("EXISTS", key)) != 0L;
		}

		public virtual long Exists(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("EXISTS", key, keysArg));
		}

		public virtual bool Expire(string key, long seconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber(MakeCommand("EXPIRE", key, seconds)) != 0L;
		}


		public virtual bool ExpireAt(string key, long timestamp)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber(MakeCommand("EXPIREAT", key, timestamp)) != 0L;
		}

		public virtual IEnumerable<string> Keys(string pattern)
		{
			if (pattern == null)
				throw new ArgumentNullException(nameof(pattern));

			if (pattern.Length == 0)
				yield break;

			foreach (RespBulk bulk in SendQueryExpectMultiBulk(MakeCommand("KEYS", pattern)))
			{
				yield return bulk.GetString(this.Encoding);
			}
		}

		public virtual bool Migrate(string host, int port, string key, int destinationDb, long timeoutMilliseconds, bool copy = false, bool replace = false, params string[] keys)
		{
			// MIGRATE host port key|"" destination-db timeout [COPY] [REPLACE] [KEYS key [key ...]]

			if (host == null)
				throw new ArgumentNullException("host");
			if (host.Length == 0 || host.All(char.IsWhiteSpace))
				throw new ArgumentOutOfRangeException("host");
			if (port <= 0)
				throw new ArgumentOutOfRangeException("port");
			if (timeoutMilliseconds < 0)
				throw new ArgumentOutOfRangeException("timeoutMilliseconds");

			var cmdWithArgs = new List<string>(9 + (keys != null ? keys.Length : 0));
			cmdWithArgs.Add("MIGRATE");
			cmdWithArgs.Add(host);
			cmdWithArgs.Add(port.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(key ?? string.Empty);
			cmdWithArgs.Add(destinationDb.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(timeoutMilliseconds.ToString(NumberFormatInfo.InvariantInfo));

			if (copy)
			{
				cmdWithArgs.Add("COPY");
			}

			if (replace)
			{
				cmdWithArgs.Add("REPLACE");
			}

			if (keys != null && keys.Length > 0)
			{
				cmdWithArgs.Add("KEYS");
				foreach (string k in keys)
				{
					if (k == null)
						throw new ArgumentOutOfRangeException(nameof(keys));
					cmdWithArgs.Add(k);
				}
			}
			return IsOk(SendQueryExpectStatement(cmdWithArgs));
		}


		public virtual bool Move(string key, int db)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (db < 0)
				throw new ArgumentOutOfRangeException(nameof(db));

			return (0L != SendQueryExpectNumber("MOVE", key, db.ToString(NumberFormatInfo.InvariantInfo)));
		}


		public virtual string Object(ObjectSubcommands subcommand, params string[] arguments)
		{
			if (arguments == null || arguments.Length < 1)
				throw new ArgumentOutOfRangeException(nameof(arguments));

			var cmdWithArgs = new List<string>(2 + arguments.Length);
			cmdWithArgs.Add("OBJECT");

			switch (subcommand)
			{
				case ObjectSubcommands.Encoding:
					cmdWithArgs.Add("ENCODING");
					break;
				case ObjectSubcommands.IdleTime:
					cmdWithArgs.Add("IDLETIME");
					break;
				case ObjectSubcommands.RefCount:
					cmdWithArgs.Add("REFCOUNT");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(subcommand));
			}

			foreach (string arg in arguments)
			{
				if (arg == null)
					throw new ArgumentOutOfRangeException(nameof(arguments));
				cmdWithArgs.Add(arg);
			}

			RespData response = SendQueryExpectStatement(cmdWithArgs);
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64().ToString(NumberFormatInfo.InvariantInfo);
			if (dataType == RespDataType.Bulk)
				return ((RespBulk)response).GetString(this.Encoding);
			throw GetUnexpectedReplyException("OBJECT");
		}


		public virtual bool Persist(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return (0L != SendQueryExpectNumber("PERSIST", key));
		}

		public virtual bool PExpire(string key, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return (0L != SendQueryExpectNumber("PEXPIRE", key, milliseconds.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual bool PExpireAt(string key, long millisecondsTimestamp)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return (0L != SendQueryExpectNumber("PEXPIREAT", key, millisecondsTimestamp.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual long PTTL(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumber("PTTL", key);
		}


		public virtual string RandomKey()
		{
			return SendQueryExpectString("RANDOMKEY");
		}

		public virtual void Rename(string key, string newkey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (newkey == null)
				throw new ArgumentNullException(nameof(newkey));

			SendQueryExpectOk("RENAME", key, newkey);
		}

		public virtual bool RenameNx(string key, string newkey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (newkey == null)
				throw new ArgumentNullException(nameof(newkey));

			return (0L != SendQueryExpectNumber("RENAMENX", key, newkey));
		}

		public virtual bool Restore(string key, long ttl, string serializedValue, bool replace = false)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (ttl < 0)
				throw new ArgumentOutOfRangeException(nameof(ttl));

			if (string.IsNullOrEmpty(serializedValue))
				throw new ArgumentOutOfRangeException(nameof(serializedValue));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("RESTORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(ttl.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(serializedValue);
			if (replace)
			{
				cmdWithArgs.Add("REPLACE");
			}
			return IsOk(SendQueryExpectStatement(cmdWithArgs));
		}


		public ScanResult<IEnumerable<string>> Scan(string pattern = null, int? count = default(int?))
		{
			return new ScanResult<IEnumerable<string>>(0, ScanInternal(pattern, count).Distinct());
		}

		protected virtual IEnumerable<string> ScanInternal(string pattern, int? count)
		{
			var cmdWithArgs = new List<string>(6);
			cmdWithArgs.Add("SCAN");
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			while (true)
			{
				RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							yield return bulk.GetString(this.Encoding);
						}
						if (cursor == 0)
							yield break;
						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("SCAN");
			}
		}

		public virtual ScanResult<IEnumerable<string>> Scan(long cursor, string pattern = null, int? count = default(int?))
		{
			var cmdWithArgs = new List<string>(6);
			cmdWithArgs.Add("SCAN");
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
			if (multiData.Length == 2)
			{
				RespData cursorData = multiData[0];
				RespData itemsData = multiData[1];
				if (cursorData.DataType == RespDataType.Bulk
					&& itemsData.DataType == RespDataType.Array
					&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
				{
					return new ScanResult<IEnumerable<string>>(cursor, CreateEnumerable((MultiBulk)itemsData));
				}
			}
			throw GetUnexpectedReplyException("SCAN");
		}

		public virtual IEnumerable<string> Sort(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			MultiBulk multibulk = SendQueryExpectMultiBulk("SORT", key);
			foreach (RespBulk item in multibulk)
			{
				yield return item.GetString(this.Encoding);
			}
		}

		public virtual IEnumerable<string> Sort(string key, SortOptions sortOptions)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (sortOptions == null)
				throw new ArgumentNullException(nameof(sortOptions));

			string[] getPatterns = sortOptions.GetPatterns;

			var cmdWithArgs = new List<string>(11 + (getPatterns != null ? getPatterns.Length << 1 : 0));
			cmdWithArgs.Add("SORT");
			cmdWithArgs.Add(key);


			if (sortOptions.SortPattern != null)
			{
				cmdWithArgs.Add("BY");
				cmdWithArgs.Add(sortOptions.SortPattern);
			}

			if (sortOptions.Limit != null)
			{
				OffsettedRange limit = sortOptions.Limit.Value;
				cmdWithArgs.Add("LIMIT");
				cmdWithArgs.Add(limit.Offset.ToString(NumberFormatInfo.InvariantInfo));
				cmdWithArgs.Add(limit.Count.ToString(NumberFormatInfo.InvariantInfo));
			}

			if (sortOptions.GetPatterns != null)
			{
				foreach (string getPattern in sortOptions.GetPatterns)
				{
					cmdWithArgs.Add("GET");
					cmdWithArgs.Add(getPattern);
				}
			}

			if (sortOptions.Desc)
			{
				cmdWithArgs.Add("DESC");
			}

			if (sortOptions.Alpha)
			{
				cmdWithArgs.Add("ALPHA");
			}

			if (sortOptions.StoreAtKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(sortOptions.StoreAtKey);

				yield return SendQueryExpectNumber(cmdWithArgs).ToString(NumberFormatInfo.InvariantInfo);
				yield break;
			}

			foreach (RespBulk bulk in SendQueryExpectMultiBulk(cmdWithArgs))
			{
				yield return bulk.GetString(this.Encoding);
			}
		}

		public virtual bool Touch(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("TOUCH", key) != 0L;
		}

		public virtual long Touch(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("TOUCH", ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual long Touch(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("TOUCH", key, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual long TTL(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumber("TTL", key);
		}

		public virtual string Type(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectString("TYPE", key);
		}

		public virtual bool Unlink(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("UNLINK", key) != 0L;
		}

		public virtual long Unlink(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("UNLINK", ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual long Unlink(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return 0L;
			return SendQueryExpectNumber(MakeCommand("UNLINK", key, ValidateKeys(keysArg, nameof(keys))));
		}
 
		public virtual long Wait(int numslaves, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));

			return SendQueryExpectNumber("WAIT", numslaves.ToString(NumberFormatInfo.InvariantInfo), timeout.ToString(NumberFormatInfo.InvariantInfo));
		}

	}
}