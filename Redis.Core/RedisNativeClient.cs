﻿using Redis.Core.Interfaces;
using RespService;
using RespService.DataTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient : RespClient, IRedisNativeClient
	{
		protected static readonly Task<long> DefaultLongTask = Task.FromResult(0L);
		protected static readonly string[] DefaultStringEnumerable = new string[0];
		private Queue<ITransactionTask> _transactionTasks = null;

		public RedisNativeClient(string hostname, int port)
			: base(hostname, port)
		{

		}

		protected bool InTransaction
		{
			get { return _transactionTasks != null; }
		}

		protected virtual void TryConnect()
		{
			if (!this.Connected)
			{
				base.Connect();
				if (_password != null)
					AuthInternal(_password);
			}
		}

		protected virtual async Task TryConnectAsync()
		{
			if (!this.Connected)
			{
				await base.ConnectAsync().ConfigureAwait(false);
				if (_password != null)
				{
					await AuthInternalAsync(_password).ConfigureAwait(false);
				}
			}
		}




		private void AuthInternal(string password)
		{
			base.SendQuery(new RespArray(new RespBulk("AUTH", this.Encoding), new RespBulk(password, this.Encoding)));
		}

		private Task AuthInternalAsync(string password)
		{
			return base.SendQueryAsync(new RespArray(new RespBulk("AUTH", this.Encoding), new RespBulk(password, this.Encoding)));
		}

		protected static IEnumerable<string> ValidateKeys(IEnumerable<string> keys, string argumentName)
		{
			foreach (string key in keys)
			{
				if (key == null)
					throw new ArgumentOutOfRangeException(argumentName);
				yield return key;
			}
		}

		protected static IEnumerable<string> MakeCommand(params object[] args)
		{
			return MakeCommand((IEnumerable)args);
		}

		protected static IEnumerable<string> MakeCommand(IEnumerable args)
		{
			foreach (object arg in args)
			{
				if (arg == null)
				{
					yield return null;
					continue;
				}
				var str = arg as string;
				if (str != null)
				{
					yield return str;
					continue;
				}
				var enu = arg as IEnumerable;
				if (enu != null)
				{
					foreach (string s in MakeCommand(enu))
						yield return s;
					continue;
				}
				yield return Convert.ToString(arg, CultureInfo.InvariantCulture);
			}
		}

		protected ICollection<string> CreateSet(MultiBulk multiBulk)
		{
			var results = new List<string>();
			foreach (RespBulk bulk in multiBulk)
			{
				results.Add(bulk.GetString(this.Encoding));
			}
			return results;
		}

		protected ICollection<string> CreateSortedSet(MultiBulk multiBulk)
		{
			var results = new List<string>();
			foreach (RespBulk bulk in multiBulk)
			{
				results.Add(bulk.GetString(this.Encoding));
			}
			return results;
		}

		protected ICollection<KeyValuePair<string, double>> CreateSortedSetWithScores(MultiBulk multiBulk)
		{
			var results = new List<KeyValuePair<string, double>>();
			bool isKey = false;
			string key = null;
			foreach (RespBulk bulk in multiBulk)
			{
				isKey = !isKey;
				if (isKey)
				{
					key = bulk.GetString(this.Encoding);
				}
				else
				{
					results.Add(new KeyValuePair<string, double>(key, Utils.StringToDouble(bulk.GetString(this.Encoding))));
				}
			}
			return results;
		}

		protected IEnumerable<string> CreateEnumerable(MultiBulk multiBulk)
		{
			foreach (RespBulk bulk in multiBulk)
			{
				yield return bulk.GetString(this.Encoding);
			}
		}

		protected Dictionary<string, string> CreateDictionary(IEnumerable<RespBulk> multiBulk)
		{
			var results = new Dictionary<string, string>();
			bool isKey = false;
			string key = null;
			foreach (RespBulk bulk in multiBulk)
			{
				isKey = !isKey;
				if (isKey)
				{
					key = bulk.GetString(this.Encoding);
				}
				else
				{
					results.Add(key, bulk.GetString(this.Encoding));
				}
			}
			return results;
		}

		protected bool IsOk(RespData response)
		{
			if (response.DataType == RespDataType.String)
			{
				if ("OK".Equals(((RespString)response).GetString(this.Encoding), StringComparison.OrdinalIgnoreCase))
					return true;
			}
			return false;
		}

		protected bool IsQueued(RespData response)
		{
			if (response.DataType == RespDataType.String)
			{
				if ("QUEUED".Equals(((RespString)response).GetString(this.Encoding), StringComparison.OrdinalIgnoreCase))
					return true;
			}
			return false;
		}

		public override RespData SendQuery(RespArray command)
		{
			TryConnect();

			RespData response = base.SendQuery(command);
			if (response.DataType == RespDataType.Error)
				throw new RedisException(((RespError)response).GetString(this.Encoding));
			return response;
		}

		protected long SendQueryExpectNumber(params string[] query)
		{
			return SendQueryExpectNumber((IEnumerable<string>)query);
		}

		protected long SendQueryExpectNumber(IEnumerable<string> query)
		{
			RespData response = SendQueryExpectStatement(query);
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64();
			if (dataType == RespDataType.String
				&& "QUEUED".Equals(((RespString)response).GetString(this.Encoding), StringComparison.OrdinalIgnoreCase))
			{
				return default(long);
			}
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected void SendQueryExpectOk(params string[] query)
		{
			SendQueryExpectOk((IEnumerable<string>)query);
		}

		protected void SendQueryExpectOk(IEnumerable<string> query)
		{
			if (!IsOk(SendQueryExpectStatement(query)))
				throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected string SendQueryExpectString(params string[] query)
		{
			return SendQueryExpectString((IEnumerable<string>)query);
		}

		protected string SendQueryExpectString(IEnumerable<string> query)
		{
			RespData response = SendQueryExpectStatement(query);
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.String)
				return ((RespString)response).GetString(this.Encoding);
			if (dataType == RespDataType.Bulk)
				return ((RespBulk)response).GetString(this.Encoding);
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected MultiBulk SendQueryExpectMultiBulk(params string[] query)
		{
			return SendQueryExpectMultiBulk((IEnumerable<string>)query);
		}

		protected MultiBulk SendQueryExpectMultiBulk(IEnumerable<string> query)
		{
			RespData response = SendQueryExpectStatement(query);
			if (response.DataType == RespDataType.Array)
				return (MultiBulk)response;
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected RespArray SendQueryExpectArray(params string[] query)
		{
			return SendQueryExpectArray((IEnumerable<string>)query);
		}

		protected RespArray SendQueryExpectArray(IEnumerable<string> query)
		{
			RespData response = SendQueryExpectStatement(query);
			if (response.DataType == RespDataType.Array)
				return (RespArray)response;
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected RespData SendQueryExpectStatement(IEnumerable<string> query)
		{
			if (query == null)
				throw new ArgumentNullException(nameof(query));

			RespData[] queryData = query.Select(s => (RespData)new RespBulk(s, this.Encoding)).ToArray();
			if (queryData.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(query));

			return SendQuery(new RespArray(queryData));
		}

		protected internal static RedisException GetUnexpectedReplyException(string name)
		{
			return new RedisException("Unexpected reply on " + name);
		}

		#region Async

		public override async Task<RespData> SendQueryAsync(RespArray command)
		{
			await TryConnectAsync().ConfigureAwait(false);

			RespData response = await base.SendQueryAsync(command).ConfigureAwait(false);
			if (response.DataType == RespDataType.Error)
				throw new RedisException(((RespError)response).GetString(this.Encoding));
			return response;
		}

		protected Task<long> SendQueryExpectNumberAsync(params string[] query)
		{
			return SendQueryExpectNumberAsync((IEnumerable<string>)query);
		}

		protected async Task<long> SendQueryExpectNumberAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64();
			if (IsQueued(response))
			{
				CreateInt64TransactionTask(query.First());
				return 0;
			}
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task<bool> SendQueryExpectBoolAsync(params string[] query)
		{
			return SendQueryExpectBoolAsync((IEnumerable<string>)query);
		}

		protected async Task<bool> SendQueryExpectBoolAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64() != 0L;
			if (IsQueued(response))
			{
				CreateBoolTransactionTask(query.First());
				return false;
			}
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task SendQueryExpectOkAsync(params string[] query)
		{
			return SendQueryExpectOkAsync((IEnumerable<string>)query);
		}

		protected async Task SendQueryExpectOkAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			if (response.DataType == RespDataType.String)
			{
				string s = ((RespString)response).GetString(this.Encoding);
				if ("OK".Equals(s, StringComparison.OrdinalIgnoreCase))
					return;
				if ("QUEUED".Equals(s, StringComparison.OrdinalIgnoreCase))
				{
					CreateVoidTransactionTask(query.First());
					return;
				}
			}
			throw new RedisException("Unexpected reply on " + query.First());
		}

		protected Task<string> SendQueryExpectStringAsync(params string[] query)
		{
			return SendQueryExpectStringAsync((IEnumerable<string>)query);
		}

		protected async Task<string> SendQueryExpectStringAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.String)
			{
				string s = ((RespString)response).GetString(this.Encoding);
				if ("QUEUED".Equals(s, StringComparison.OrdinalIgnoreCase))
				{
					CreateStringTransactionTask(query.First());
					return null;
				}
				return s;
			}
			if (dataType == RespDataType.Bulk)
				return ((RespBulk)response).GetString(this.Encoding);
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task<double?> SendQueryExpectNullableDoubleAsync(params string[] query)
		{
			return SendQueryExpectNullableDoubleAsync((IEnumerable<string>)query);
		}

		protected async Task<double?> SendQueryExpectNullableDoubleAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			RespDataType dataType = response.DataType;
			string s;
			if (dataType == RespDataType.String)
			{
				s = ((RespString)response).GetString(this.Encoding);
				if ("QUEUED".Equals(s, StringComparison.OrdinalIgnoreCase))
				{
					CreateNullableDoubleTransactionTask(query.First());
					return null;
				}
				return Utils.StringToDouble(s);
			}
			if (dataType == RespDataType.Bulk)
			{
				s = ((RespBulk)response).GetString(this.Encoding);
				return s is null ? default(double?) : Utils.StringToDouble(s);
			}
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task<MultiBulk?> SendQueryExpectMultiBulkAsync(params string[] query)
		{
			return SendQueryExpectMultiBulkAsync((IEnumerable<string>)query);
		}

		protected async Task<MultiBulk?> SendQueryExpectMultiBulkAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			if (response.DataType == RespDataType.Array)
				return (MultiBulk)response;
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task<RespArray?> SendQueryExpectArrayAsync(params string[] query)
		{
			return SendQueryExpectArrayAsync((IEnumerable<string>)query);
		}

		protected async Task<RespArray?> SendQueryExpectArrayAsync(IEnumerable<string> query)
		{
			RespData response = await SendQueryExpectStatementAsync(query).ConfigureAwait(false);
			if (response.DataType == RespDataType.Array)
				return (RespArray)response;
			throw new RedisException("Unexpected reply on " + query.FirstOrDefault());
		}

		protected Task<RespData> SendQueryExpectStatementAsync(IEnumerable<string> query)
		{
			if (query == null)
				throw new ArgumentNullException(nameof(query));

			RespData[] queryData = query.Select(s => (RespData)new RespBulk(s, this.Encoding)).ToArray();
			if (queryData.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(query));
			return SendQueryAsync(new RespArray(queryData));
		}

		#endregion
	}
}