﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual bool SAdd(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return (1L == SendQueryExpectNumber("SADD", key, member));
		}

		public virtual long SAdd(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			return SendQueryExpectNumber(MakeCommand("SADD", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual long SAdd(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("SADD");
			cmdWithArgs.Add(key);
			cmdWithArgs.AddRange(ValidateKeys(members, nameof(members)));
			if (cmdWithArgs.Count == 2)
				return 0;
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long SCard(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("SCARD", key);
		}

		public virtual ICollection<string> SDiff(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSet(SendQueryExpectMultiBulk("SDIFF", key));
		}

		public virtual ICollection<string> SDiff(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SDIFF", key, ValidateKeys(keys, nameof(keys)))));
		}

		public virtual ICollection<string> SDiff(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SDIFF", ValidateKeys(keysArg, nameof(keys)))));
		}

		public virtual long SDiffStore(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("SDIFFSTORE", destination, key);
		}

		public virtual long SDiffStore(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SDIFFSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual long SDiffStore(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SDIFFSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual ICollection<string> SInter(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSet(SendQueryExpectMultiBulk("SINTER", key));
		}

		public virtual ICollection<string> SInter(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SINTER", key, ValidateKeys(keys, nameof(keys)))));
		}

		public virtual ICollection<string> SInter(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SINTER", ValidateKeys(keysArg, nameof(keys)))));
		}

		public virtual long SInterStore(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("SINTERSTORE", destination, key);
		}

		public virtual long SInterStore(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SINTERSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual long SInterStore(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SINTERSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

		public virtual bool SIsMember(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return (0L != SendQueryExpectNumber("SISMEMBER", key, member));
		}

		public virtual ICollection<string> SMembers(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSet(SendQueryExpectMultiBulk("SMEMBERS", key));
		}

		public virtual bool SMove(string source, string destination, string member)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return 0L != SendQueryExpectNumber("SMOVE", source, destination, member);
		}

		public virtual string SPop(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("SPOP", key);
		}

		public virtual ICollection<string> SPop(string key, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			return CreateSet(SendQueryExpectMultiBulk("SPOP", key, count.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual string SRandMember(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("SRANDMEMBER", key);
		}

		public virtual ICollection<string> SRandMember(string key, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			return CreateSet(SendQueryExpectMultiBulk("SRANDMEMBER", key, count.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual long SRem(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectNumber("SREM", key, member);
		}

		public virtual long SRem(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			return SendQueryExpectNumber(MakeCommand("SREM", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual long SRem(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			string[] membersArg = members.ToArray();
			if (membersArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(members));
			return SendQueryExpectNumber(MakeCommand("SREM", key, ValidateKeys(membersArg, nameof(members))));
		}

		public virtual ScanResult<ICollection<string>> SScan(string key, long cursor, string pattern = null, int? count = default(int?))
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("SSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
			if (multiData.Length == 2)
			{
				RespData cursorData = multiData[0];
				RespData itemsData = multiData[1];
				if (cursorData.DataType == RespDataType.Bulk
					&& itemsData.DataType == RespDataType.Array
					&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
				{
					return new ScanResult<ICollection<string>>(cursor, CreateSet((MultiBulk)itemsData));
				}
			}
			throw GetUnexpectedReplyException("SSCAN");
		}

		public virtual ScanResult<ICollection<string>> SScan(string key, string pattern = null, int? count = default(int?))
		{
			return new ScanResult<ICollection<string>>(0, new HashSet<string>(SScanInternal(key, pattern, count)));
		}

		protected virtual IEnumerable<string> SScanInternal(string key, string pattern, int? count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("SSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			while (true)
			{
				RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							yield return bulk.GetString(this.Encoding);
						}
						if (cursor == 0)
							yield break;
						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("SSCAN");
			}
		}

		public virtual ICollection<string> SUnion(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSet(SendQueryExpectMultiBulk("SUNION", key));
		}

		public virtual ICollection<string> SUnion(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SUNION", key, keys)));
		}

		public virtual ICollection<string> SUnion(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return new HashSet<string>();
			return CreateSet(SendQueryExpectMultiBulk(MakeCommand("SUNION", keysArg)));
		}

		public virtual long SUnionStore(string destination, string key)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("SUNIONSTORE", destination, key);
		}

		public virtual long SUnionStore(string destination, string key, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SUNIONSTORE", destination, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual long SUnionStore(string destination, IEnumerable<string> keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("SUNIONSTORE", destination, ValidateKeys(keysArg, nameof(keys))));
		}

	}
}
