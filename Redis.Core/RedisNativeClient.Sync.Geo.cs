﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual bool GeoAdd(string key, double longitude, double latitude, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (GeoPosition.IsValidLongitude(longitude))
				throw new ArgumentOutOfRangeException(nameof(longitude));
			if (GeoPosition.IsValidLatitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return 0L != SendQueryExpectNumber("GEOADD", key, longitude.ToString(NumberFormatInfo.InvariantInfo), latitude.ToString(NumberFormatInfo.InvariantInfo), member);
		}

		public virtual bool GeoAdd(string key, GeoObject geo)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (geo.Name != null && !geo.Position.IsValid())
				throw new ArgumentOutOfRangeException(nameof(geo));
			return 0L != SendQueryExpectNumber("GEOADD", key, geo.Position.Longitude.ToString(NumberFormatInfo.InvariantInfo), geo.Position.Latitude.ToString(NumberFormatInfo.InvariantInfo), geo.Name);

		}

		public virtual long GeoAdd(string key, IEnumerable<GeoObject> geo)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (geo == null)
				throw new ArgumentNullException(nameof(geo));
			GeoObject[] geodata = geo.ToArray();
			if (geodata.Length == 0)
				return 0L;
			var records = new List<string>(geodata.Length * 3);
			foreach (GeoObject item in geodata)
			{
				if (item.Name != null && !item.Position.IsValid())
					throw new ArgumentOutOfRangeException(nameof(geo));
				records.Add(item.Name);
				records.Add(item.Position.Longitude.ToString(NumberFormatInfo.InvariantInfo));
				records.Add(item.Position.Latitude.ToString(NumberFormatInfo.InvariantInfo));
			}
			return SendQueryExpectNumber(MakeCommand("GEOADD", key, records));
		}

		public virtual string GeoHash(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectMultiBulk("GEOHASH", key, member).First().GetString(this.Encoding);
		}

		public virtual string[] GeoHash(string key, string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			if (members.Length == 0)
				return new string[0];
			return SendQueryExpectMultiBulk(MakeCommand("GEOHASH", key, ValidateKeys(members, nameof(members)))).Select(bulk => bulk.GetString(this.Encoding)).ToArray();
		}

		public virtual GeoObject? GeoPos(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			var item = (RespArray)SendQueryExpectArray("GEOPOS", key, member).First();
			if (item.IsNull())
				return null;
			var geoInfo = item.ToArray();
			if (geoInfo.Length != 2 || geoInfo[0].DataType != RespDataType.Bulk || geoInfo[1].DataType != RespDataType.Bulk)
				throw GetUnexpectedReplyException("GEOPOS");

			return new GeoObject(member, new GeoPosition(
					StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
					StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
				));
		}

		public virtual IEnumerable<GeoObject> GeoPos(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));

			int index = -1;
			foreach (RespArray item in SendQueryExpectArray(MakeCommand("GEOPOS", key, member, ValidateKeys(members, nameof(members)))))
			{
				if (item.IsNull())
				{
					index++;
					continue;
				}

				var geoInfo = item.ToArray();
				if (geoInfo.Length != 2 || geoInfo[0].DataType != RespDataType.Bulk || geoInfo[1].DataType != RespDataType.Bulk)
					throw GetUnexpectedReplyException("GEOPOS");

				yield return new GeoObject(index++ == -1 ? member : members[index],
					new GeoPosition(
						StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
						StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
				));
			}
		}

		public virtual IEnumerable<GeoObject> GeoPos(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			IList<string> membersArg = members as IList<string> ?? members.ToArray();
			if (membersArg.Count == 0)
				throw new ArgumentOutOfRangeException(nameof(members));

			int index = 0;
			foreach (RespArray item in SendQueryExpectArray(MakeCommand("GEOPOS", key, ValidateKeys(members, nameof(members)))))
			{
				if (item.IsNull())
				{
					index++;
					continue;
				}

				var geoInfo = item.ToArray();
				if (geoInfo.Length != 2)
					throw GetUnexpectedReplyException("GEOPOS");

				yield return new GeoObject(membersArg[index],
					new GeoPosition(
						StringToDouble(((RespBulk)geoInfo[0]).GetString(this.Encoding)),
						StringToDouble(((RespBulk)geoInfo[1]).GetString(this.Encoding))
				));
			}
		}

		public virtual double? GeoDist(string key, string member1, string member2)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member1 == null)
				throw new ArgumentNullException(nameof(member1));
			if (member2 == null)
				throw new ArgumentNullException(nameof(member2));

			string bulk = SendQueryExpectString("GEODIST", key, member1, member2);
			return bulk != null ? StringToDouble(bulk) : default(double?);
		}

		public virtual double? GeoDist(string key, string member1, string member2, GeoUnits unit)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member1 == null)
				throw new ArgumentNullException(nameof(member1));
			if (member2 == null)
				throw new ArgumentNullException(nameof(member2));

			string bulk = SendQueryExpectString("GEODIST", key, member1, member2, GeoUnitToString(unit));
			return bulk != null ? StringToDouble(bulk) : default(double?);
		}

		public virtual IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			return ParseRadiusResult("GEORADIUS", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			return ParseRadiusResult("GEORADIUS", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUS", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUS", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			return ParseRadiusResult("GEORADIUS_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			return ParseRadiusResult("GEORADIUS_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUS_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUS_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (!GeoPosition.IsValidLongitude(longtitude))
				throw new ArgumentOutOfRangeException(nameof(longtitude));
			if (!GeoPosition.IsValidLongitude(latitude))
				throw new ArgumentOutOfRangeException(nameof(latitude));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(13);
			cmdWithArgs.Add("GEORADIUS");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(longtitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(latitude.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			return ParseRadiusResult("GEORADIUSBYMEMBER", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			return ParseRadiusResult("GEORADIUSBYMEMBER", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUSBYMEMBER", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUSBYMEMBER", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			if (storeKey == null && storeDistKey == null)
				throw new ArgumentNullException(nameof(storeKey));

			var cmdWithArgs = new List<string>(12);
			cmdWithArgs.Add("GEORADIUSBYMEMBER");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");

			if (storeKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(storeKey);
			}
			if (storeDistKey != null)
			{
				cmdWithArgs.Add("STOREDIST");
				cmdWithArgs.Add(storeDistKey);
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			return ParseRadiusResult("GEORADIUSBYMEMBER_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(10);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			return ParseRadiusResult("GEORADIUSBYMEMBER_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));

			var cmdWithArgs = new List<string>(9);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUSBYMEMBER_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		public virtual IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (radius < 0)
				throw new ArgumentOutOfRangeException(nameof(radius));
			if (count <= 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			var cmdWithArgs = new List<string>(11);
			cmdWithArgs.Add("GEORADIUSBYMEMBER_RO");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(member);
			cmdWithArgs.Add(radius.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(GeoUnitToString(unit));
			if (withCoord) cmdWithArgs.Add("WITHCOORD");
			if (withDist) cmdWithArgs.Add("WITHDIST");
			if (withHash) cmdWithArgs.Add("WITHHASH");
			cmdWithArgs.Add("COUNT");
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(order == ListSortDirection.Descending ? "DESC" : "ASC");
			return ParseRadiusResult("GEORADIUSBYMEMBER_RO", SendQueryExpectArray(cmdWithArgs), withCoord, withDist, withHash);
		}

		private IEnumerable<GeoData> ParseRadiusResult(string funcname, RespArray result, bool withCoord, bool withDist, bool withHash)
		{
			if (!withCoord && !withDist && !withHash)
			{
				foreach (string name in CreateEnumerable(result))
				{
					yield return new GeoData(name);
				}
				yield break;
			}

			foreach (RespData item in result)
			{
				if (item.DataType != RespDataType.Array)
					throw GetUnexpectedReplyException(funcname);

				GeoData geodata;
				try
				{
					int index = 0;
					RespData[] data = ((RespArray)item).ToArray();

					if (data[index].DataType != RespDataType.Bulk)
						throw GetUnexpectedReplyException(funcname);
					geodata = new GeoData(((RespBulk)data[index++]).GetString(this.Encoding));

					if (withDist)
					{
						if (data[index].DataType != RespDataType.Bulk)
							throw GetUnexpectedReplyException(funcname);
						geodata.Distance = StringToDouble(((RespBulk)data[index++]).GetString(this.Encoding));
					}
					if (withHash)
					{
						if (data[index].DataType != RespDataType.Integer)
							throw GetUnexpectedReplyException(funcname);
						geodata.Hash = ((RespInteger)data[index++]).GetInt64();
					}
					if (withCoord)
					{
						if (data[index].DataType != RespDataType.Array)
							throw GetUnexpectedReplyException(funcname);
						RespData[] coord = ((RespArray)data[index]).ToArray();
						if (coord.Length != 2 || coord[0].DataType != RespDataType.Bulk || coord[1].DataType != RespDataType.Bulk)
							throw GetUnexpectedReplyException(funcname);
						geodata.Location = new GeoPosition(StringToDouble(((RespBulk)coord[0]).GetString(this.Encoding)), StringToDouble(((RespBulk)coord[1]).GetString(this.Encoding)));
					}
				}
				catch (IndexOutOfRangeException)
				{
					throw GetUnexpectedReplyException(funcname);
				}
				yield return geodata;
			}
		}

	}
}
