﻿using Redis.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Redis.Core
{
	public class RedisClient : RedisNativeClient, IRedisClient, IPooledRedisClient
	{

		public RedisClient(string hostname, int port)
			: this(hostname, port, null, 0)
		{

		}

		public RedisClient(string hostname, int port, string password, int database)
			: base(hostname, port)
		{
			this.EndPoint = new RedisEndPoint(hostname, port, password, database);
		}

		public RedisClient(RedisEndPoint endPoint)
			: base(endPoint.Host, endPoint.Port)
		{
			this.EndPoint = endPoint;
		}

		protected RedisEndPoint EndPoint { get; private set; }

		void IPooledRedisClient.AuthWithDefaultPassword()
		{
			if (this.EndPoint.RequiresAuth)
			{
				Auth(this.EndPoint.Password);
			}
		}

		Task IPooledRedisClient.AuthWithDefaultPasswordAsync()
		{
			return this.EndPoint.RequiresAuth ? AuthAsync(this.EndPoint.Password) : DefaultLongTask;
		}

		void IPooledRedisClient.SelectDefaultDatabase()
		{
			if (this.Database != this.EndPoint.Database)
			{
				Select(this.EndPoint.Database);
			}
		}

		Task IPooledRedisClient.SelectDefaultDatabaseAsync()
		{
			return this.Database != this.EndPoint.Database ? SelectAsync(this.EndPoint.Database) : DefaultLongTask;
		}

		void IPooledRedisClient.ResetTransaction()
		{
			TryConnect();
			CleanTransactionTasks();
			base.SendQuery("DISCARD");
		}

		async Task IPooledRedisClient.ResetTransactionAsync()
		{
			await TryConnectAsync().ConfigureAwait(false);
			CleanTransactionTasks();
			await base.SendQueryAsync("DISCARD").ConfigureAwait(false);
		}

	}
}
