﻿namespace Redis.Core
{
	public struct OffsettedRange
	{
		public int Offset { get; set; }
		public int Count { get; set; }
	}
}
