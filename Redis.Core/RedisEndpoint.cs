﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Redis.Core
{
	public class RedisEndPoint : DnsEndPoint
	{
		public static readonly string DefaultHost = "localhost";
		public static readonly int DefaultPort = 6379;
		public static readonly int DefaultDatabase = 0;

		private int _hashCode;
		private readonly int _database;
		private readonly AddressFamily _addressFamily;

		public static RedisEndPoint Parse(string endPointString)
		{
			if (string.IsNullOrWhiteSpace(endPointString))
				throw new ArgumentException("endPointString");

			string host;
			int port;
			int db = RedisEndPoint.DefaultDatabase;
			string password = null;

			int startPos = endPointString.LastIndexOf('@');
			if (startPos != -1)
			{
				password = endPointString.Remove(startPos);
				endPointString = endPointString.Substring(startPos + 1);
				startPos = password.IndexOf(':');
				if (startPos != -1)
				{
					string dbStr = password.Remove(startPos);
					if (dbStr.All(char.IsDigit))
					{
						db = int.Parse(dbStr);
						password = password.Substring(startPos + 1);
					}
				}
			}
			bool isIPv6;
			if (endPointString.Length > 0 && endPointString[0] == '[')
			{
				isIPv6 = true;
				startPos = endPointString.IndexOf(']') + 1;
			}
			else
			{
				isIPv6 = false;
				startPos = 0;
			}
			startPos = endPointString.IndexOf(':', startPos);
			if (startPos != -1)
			{
				host = endPointString.Remove(startPos);
				if (!isIPv6 && (host.Contains(':') || endPointString.Contains("::")))
				{
					host = endPointString;
					port = RedisEndPoint.DefaultPort;
				}
				else
				{
					port = int.Parse(endPointString.Substring(startPos + 1));
				}
			}
			else
			{
				host = endPointString;
				port = RedisEndPoint.DefaultPort;
			}
			return new RedisEndPoint(host, port, password, db);
		}

		public RedisEndPoint()
			: this(DefaultHost, DefaultPort, null, DefaultDatabase)
		{

		}

		public RedisEndPoint(string host, int port)
			: this(host, port, null, DefaultDatabase)
		{

		}

		public RedisEndPoint(string host, int port, string password, int database)
			: base(host.Trim('[', ']'), port)
		{
			if (database < 0)
				throw new ArgumentOutOfRangeException("database");

			IPAddress ipAddress;
			_addressFamily = IPAddress.TryParse(host, out ipAddress) ? ipAddress.AddressFamily : base.AddressFamily;
			_database = database;
			this.Password = password;

			_hashCode = this.ToString().GetHashCode();
		}

		public override AddressFamily AddressFamily
		{
			get
			{
				return _addressFamily;
			}
		}

		public int Database
		{
			get { return _database; }
		}

		public string Password { get; }

		public bool RequiresAuth
		{
			get { return !string.IsNullOrEmpty(Password); }
		}

		public override int GetHashCode()
		{
			return _hashCode;
		}

		public override bool Equals(object obj)
		{
			var other = obj as RedisEndPoint;
			return other != null && this.GetHashCode() == other.GetHashCode() && string.Equals(this.ToString(), other.ToString(), StringComparison.Ordinal);
		}

		public override string ToString()
		{
			return string.Format("{0}{1}{2}{3}",
				this.Database != RedisEndPoint.DefaultDatabase ? this.Database + ":" : string.Empty,
				this.RequiresAuth ? this.Password + "@" : string.Empty,
				this.AddressFamily == AddressFamily.InterNetworkV6 ? "[" + this.Host + "]" : this.Host,
				this.Port > 0 && this.Port != DefaultPort ? ":" + this.Port.ToString() : string.Empty
			);
		}

	}
}