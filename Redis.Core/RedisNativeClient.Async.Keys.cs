﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<bool> DelAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync("DEL", key);
		}

		public virtual Task<long> DelAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("DEL", keysArg));
		}

		public virtual Task<long> DelAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("DEL", key, keysArg));
		}

		public virtual Task<string> DumpAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("DUMP", key);
		}

		public virtual Task<long> ExistsAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("EXISTS", keysArg));
		}

		public virtual Task<bool> ExistsAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync(MakeCommand("EXISTS", key));
		}

		public virtual Task<long> ExistsAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("EXISTS", key, keysArg));
		}

		public virtual Task<bool> ExpireAsync(string key, long seconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync(MakeCommand("EXPIRE", key, seconds));
		}


		public virtual Task<bool> ExpireAtAsync(string key, long timestamp)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync(MakeCommand("EXPIREAT", key, timestamp));
		}

		public virtual async Task<IEnumerable<string>> KeysAsync(string pattern)
		{
			if (pattern == null)
				throw new ArgumentNullException(nameof(pattern));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("KEYS", pattern)).ConfigureAwait(false);
			if (response.HasValue)
				return CreateEnumerable(response.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual async Task<bool> MigrateAsync(string host, int port, string key, int destinationDb, long timeoutMilliseconds, bool copy = false, bool replace = false, params string[] keys)
		{
			// MIGRATE host port key|"" destination-db timeout [COPY] [REPLACE] [KEYS key [key ...]]

			if (host == null)
				throw new ArgumentNullException("host");
			if (host.Length == 0 || host.All(char.IsWhiteSpace))
				throw new ArgumentOutOfRangeException("host");
			if (port <= 0)
				throw new ArgumentOutOfRangeException("port");
			if (timeoutMilliseconds < 0)
				throw new ArgumentOutOfRangeException("timeoutMilliseconds");

			var cmdWithArgs = new List<string>(9 + (keys != null ? keys.Length : 0));
			cmdWithArgs.Add("MIGRATE");
			cmdWithArgs.Add(host);
			cmdWithArgs.Add(port.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(key ?? string.Empty);
			cmdWithArgs.Add(destinationDb.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(timeoutMilliseconds.ToString(NumberFormatInfo.InvariantInfo));

			if (copy)
			{
				cmdWithArgs.Add("COPY");
			}

			if (replace)
			{
				cmdWithArgs.Add("REPLACE");
			}

			if (keys != null && keys.Length > 0)
			{
				cmdWithArgs.Add("KEYS");
				foreach (string k in keys)
				{
					if (k == null)
						throw new ArgumentOutOfRangeException(nameof(keys));
					cmdWithArgs.Add(k);
				}
			}
			RespData response = await SendQueryExpectStatementAsync(cmdWithArgs).ConfigureAwait(false);
			if (IsQueued(response))
			{
				CreateTransactionTask<RespData, bool>(IsOk);
				return false;
			}
			return IsOk(response);
		}


		public virtual Task<bool> MoveAsync(string key, int db)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (db < 0)
				throw new ArgumentOutOfRangeException(nameof(db));

			return SendQueryExpectBoolAsync("MOVE", key, db.ToString(NumberFormatInfo.InvariantInfo));
		}


		public virtual async Task<string> ObjectAsync(ObjectSubcommands subcommand, params string[] arguments)
		{
			if (arguments == null || arguments.Length < 1)
				throw new ArgumentOutOfRangeException(nameof(arguments));

			var cmdWithArgs = new List<string>(2 + arguments.Length);
			cmdWithArgs.Add("OBJECT");

			switch (subcommand)
			{
				case ObjectSubcommands.Encoding:
					cmdWithArgs.Add("ENCODING");
					break;
				case ObjectSubcommands.IdleTime:
					cmdWithArgs.Add("IDLETIME");
					break;
				case ObjectSubcommands.RefCount:
					cmdWithArgs.Add("REFCOUNT");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(subcommand));
			}

			foreach (string arg in arguments)
			{
				if (arg == null)
					throw new ArgumentOutOfRangeException(nameof(arguments));
				cmdWithArgs.Add(arg);
			}

			RespData response = (await SendQueryExpectStatementAsync(cmdWithArgs).ConfigureAwait(false));
			RespDataType dataType = response.DataType;
			if (dataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64().ToString(NumberFormatInfo.InvariantInfo);
			if (dataType == RespDataType.Bulk)
				return ((RespBulk)response).GetString(this.Encoding);
			throw GetUnexpectedReplyException("OBJECT");
		}


		public virtual Task<bool> PersistAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectBoolAsync("PERSIST", key);
		}

		public virtual Task<bool> PExpireAsync(string key, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectBoolAsync("PEXPIRE", key, milliseconds.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<bool> PExpireAtAsync(string key, long millisecondsTimestamp)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectBoolAsync("PEXPIREAT", key, millisecondsTimestamp.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<long> PTTLAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumberAsync("PTTL", key);
		}


		public virtual Task<string> RandomKeyAsync()
		{
			return SendQueryExpectStringAsync("RANDOMKEY");
		}

		public virtual async Task RenameAsync(string key, string newkey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (newkey == null)
				throw new ArgumentNullException(nameof(newkey));

			await SendQueryExpectOkAsync("RENAME", key, newkey);
		}

		public virtual Task<bool> RenameNxAsync(string key, string newkey)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (newkey == null)
				throw new ArgumentNullException(nameof(newkey));

			return SendQueryExpectBoolAsync("RENAMENX", key, newkey);
		}

		public virtual async Task<bool> RestoreAsync(string key, long ttl, string serializedValue, bool replace = false)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (ttl < 0)
				throw new ArgumentOutOfRangeException(nameof(ttl));

			if (string.IsNullOrEmpty(serializedValue))
				throw new ArgumentOutOfRangeException(nameof(serializedValue));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("RESTORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(ttl.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(serializedValue);
			if (replace)
			{
				cmdWithArgs.Add("REPLACE");
			}
			return IsOk(await SendQueryExpectStatementAsync(cmdWithArgs).ConfigureAwait(false));
		}


		public virtual async Task<ScanResult<IEnumerable<string>>> ScanAsync(string pattern = null, int? count = default(int?))
		{
			var cmdWithArgs = new List<string>(6);
			cmdWithArgs.Add("SCAN");
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			var result = new List<string>();
			while (true)
			{
				RespArray? response = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
				if (!response.HasValue)
					throw GetUnexpectedReplyException("SCAN");

				RespData[] multiData = response.Value.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1]; 
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							result.Add(bulk.GetString(this.Encoding));
						}

						if (cursor == 0)
							return new ScanResult<IEnumerable<string>>(0, result);

						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}

				}
				throw GetUnexpectedReplyException("SCAN");
			}
		}

		public virtual async Task<ScanResult<IEnumerable<string>>> ScanAsync(long cursor, string pattern = null, int? count = default(int?))
		{
			var cmdWithArgs = new List<string>(6);
			cmdWithArgs.Add("SCAN");
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespArray? response2 = await SendQueryExpectArrayAsync(cmdWithArgs).ConfigureAwait(false);
			if (response2.HasValue)
				return ParseScanResult(response2.Value);
			CreateTransactionTask<RespArray, ScanResult<IEnumerable<string>>>(ParseScanResult);
			return default;

			ScanResult<IEnumerable<string>> ParseScanResult(RespArray response)
			{
				RespData[] multiData = response.ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
					{
						return new ScanResult<IEnumerable<string>>(cursor, CreateEnumerable((MultiBulk)itemsData));
					}
				}
				throw GetUnexpectedReplyException("SCAN");
			}
		}
		public virtual async Task<IEnumerable<string>> SortAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			MultiBulk? response = await SendQueryExpectMultiBulkAsync("SORT", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateEnumerable(response.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual async Task<IEnumerable<string>> SortAsync(string key, SortOptions sortOptions)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (sortOptions == null)
				throw new ArgumentNullException(nameof(sortOptions));

			string[] getPatterns = sortOptions.GetPatterns;

			var cmdWithArgs = new List<string>(11 + (getPatterns != null ? getPatterns.Length << 1 : 0));
			cmdWithArgs.Add("SORT");
			cmdWithArgs.Add(key);


			if (sortOptions.SortPattern != null)
			{
				cmdWithArgs.Add("BY");
				cmdWithArgs.Add(sortOptions.SortPattern);
			}

			if (sortOptions.Limit != null)
			{
				OffsettedRange limit = sortOptions.Limit.Value;
				cmdWithArgs.Add("LIMIT");
				cmdWithArgs.Add(limit.Offset.ToString(NumberFormatInfo.InvariantInfo));
				cmdWithArgs.Add(limit.Count.ToString(NumberFormatInfo.InvariantInfo));
			}

			if (sortOptions.GetPatterns != null)
			{
				foreach (string getPattern in sortOptions.GetPatterns)
				{
					cmdWithArgs.Add("GET");
					cmdWithArgs.Add(getPattern);
				}
			}

			if (sortOptions.Desc)
			{
				cmdWithArgs.Add("DESC");
			}

			if (sortOptions.Alpha)
			{
				cmdWithArgs.Add("ALPHA");
			}

			if (sortOptions.StoreAtKey != null)
			{
				cmdWithArgs.Add("STORE");
				cmdWithArgs.Add(sortOptions.StoreAtKey);

				RespData response2 = await SendQueryExpectStatementAsync(cmdWithArgs).ConfigureAwait(false);
				if (response2.DataType == RespDataType.Integer)
				{
					return new string[] { ((RespInteger)response2).GetInt64().ToString(NumberFormatInfo.InvariantInfo) };
				}
				else if (IsQueued(response2))
				{
					CreateTransactionTask<RespData, IEnumerable<string>>(StoreSort);
					return null;

					IEnumerable<string> StoreSort(RespData response)
					{
						if (response.DataType == RespDataType.Integer)
							return new string[] { ((RespInteger)response).GetInt64().ToString(NumberFormatInfo.InvariantInfo) };
						throw GetUnexpectedReplyException("SORT");
					}
				}
			}

			MultiBulk? response3 = await SendQueryExpectMultiBulkAsync(cmdWithArgs).ConfigureAwait(false);
			if (response3.HasValue)
				return CreateEnumerable(response3.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual Task<bool> TouchAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync("TOUCH", key);
		}

		public virtual Task<long> TouchAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("TOUCH", keysArg));
		}

		public virtual Task<long> TouchAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("TOUCH", key, keysArg));
		}

		public virtual Task<long> TTLAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumberAsync("TTL", key);
		}

		public virtual Task<string> TypeAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectStringAsync("TYPE", key);
		}

		public virtual Task<bool> UnlinkAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync("UNLINK", key);
		}

		public virtual Task<long> UnlinkAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("UNLINK", keysArg));
		}

		public virtual Task<long> UnlinkAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return DefaultLongTask;
			return SendQueryExpectNumberAsync(MakeCommand("UNLINK", key, keysArg));
		}

		public virtual Task<long> WaitAsync(int numslaves, long timeout)
		{
			if (timeout < 0L)
				throw new ArgumentOutOfRangeException(nameof(timeout));

			return SendQueryExpectNumberAsync("WAIT", numslaves.ToString(NumberFormatInfo.InvariantInfo), timeout.ToString(NumberFormatInfo.InvariantInfo));
		}

	}
}