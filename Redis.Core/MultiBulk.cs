﻿using RespService.DataTypes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Collections;

namespace Redis.Core
{
	[StructLayout(LayoutKind.Explicit)]
	public struct MultiBulk : IEnumerable<RespBulk>
	{
		[FieldOffset(0)]
		private readonly int _offset;
		[FieldOffset(4)]
		private readonly int _count;
		[FieldOffset(8)]
		private readonly byte[] _buffer;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private MultiBulk(byte[] buffer, int offset, int count)
		{
			_offset = offset;
			_count = count;
			_buffer = buffer;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public MultiBulk(RespArray array)
			: this((RespData)array)
		{

		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private MultiBulk(RespData data)
		{
			ArraySegment<byte> segment = data.GetBuffer();
			_offset = segment.Offset;
			_count = segment.Count;
			_buffer = segment.Array;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespData(MultiBulk multibulk)
		{
			return new RespData(multibulk._buffer, multibulk._offset, multibulk._count);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator MultiBulk(RespArray array)
		{
			return new MultiBulk(array);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator RespArray(MultiBulk multibulk)
		{
			return (RespArray)(new RespData(multibulk._buffer, multibulk._offset, multibulk._count));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator MultiBulk(RespData data)
		{
			return new MultiBulk(data);
		}


		public IEnumerator<RespBulk> GetEnumerator()
		{
			foreach (RespData item in ((RespArray)this))
			{
				if (item.DataType != RespDataType.Bulk)
					throw new RedisException("MultiBulk reply contains non bulk parts.");
				yield return (RespBulk)item;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
