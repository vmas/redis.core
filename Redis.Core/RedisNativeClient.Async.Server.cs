﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public Task DebugSleep(double seconds)
		{
			if (seconds < 0.0)
				throw new ArgumentOutOfRangeException(nameof(seconds));
			return SendQueryExpectOkAsync("DEBUG", "SLEEP", seconds.ToString(NumberFormatInfo.InvariantInfo));
		}
    }
}
