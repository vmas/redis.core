﻿namespace Redis.Core
{
	public enum ExistenceMode
	{
		Default = 0,
		Add = 1,
		Update = 2,
		XX = Update,
		NX = Add,

	}
}
