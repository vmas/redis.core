﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<bool> PfAddAsync(string key, string element)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (element == null)
				throw new ArgumentNullException(nameof(element));
			return SendQueryExpectBoolAsync("PFADD", key, element);
		}
		public virtual Task<bool> PfAddAsync(string key, string element, params string[] elements)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (element == null)
				throw new ArgumentNullException(nameof(element));
			if (elements == null)
				throw new ArgumentNullException(nameof(elements));
			return SendQueryExpectBoolAsync(MakeCommand("PFADD", key, element, ValidateKeys(elements, nameof(elements))));
		}

		public virtual Task<bool> PfAddAsync(string key, IEnumerable<string> elements)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (elements == null)
				throw new ArgumentNullException(nameof(elements));
			IList<string> elementsArg = elements as IList<string> ?? elements.ToArray();
			return SendQueryExpectBoolAsync(MakeCommand("PFADD", key, ValidateKeys(elementsArg, nameof(elements))));
		}

		public virtual Task<long> PfCountAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("PFCOUNT", key);
		}

		public virtual Task<long> PfCountAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("PFCOUNT", key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task PfMergeAsync(string destkey, string sourcekey)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekey == null)
				throw new ArgumentNullException(nameof(sourcekey));
			return SendQueryExpectOkAsync("PFMERGE", destkey, sourcekey);
		}

		public virtual Task PfMergeAsync(string destkey, string sourcekey, params string[] sourcekeys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekey == null)
				throw new ArgumentNullException(nameof(sourcekey));
			if (sourcekeys == null)
				throw new ArgumentNullException(nameof(sourcekeys));
			return SendQueryExpectOkAsync(MakeCommand("PFMERGE", destkey, sourcekey, ValidateKeys(sourcekeys, nameof(sourcekeys))));
		}

		public virtual Task PfMergeAsync(string destkey, IEnumerable<string> sourcekeys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekeys == null)
				throw new ArgumentNullException(nameof(sourcekeys));
			IList<string> sourcekeysArg = sourcekeys as IList<string> ?? sourcekeys.ToArray();
			return SendQueryExpectOkAsync(MakeCommand("PFMERGE", destkey, ValidateKeys(sourcekeysArg, nameof(sourcekeys))));
		}

	}
}
