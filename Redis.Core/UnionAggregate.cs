﻿namespace Redis.Core
{
	public enum UnionAggregate
	{
		Sum = 0,
		Min = 1,
		Max = 2,
		Default = Sum
	}
}
