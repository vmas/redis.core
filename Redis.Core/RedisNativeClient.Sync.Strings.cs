﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual long Append(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("APPEND", key, value);
		}

		public virtual long BitCount(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("BITCOUNT", key);
		}

		public virtual long BitCount(string key, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("BITCOUNT", key, start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		//TODO: BITFIELD key [GET type offset] [SET type offset value] [INCRBY type offset increment] [OVERFLOW WRAP|SAT|FAIL]

		public virtual long BitOp(BitOpType operation, string destkey, string key)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumber("BITOP", BitOpTypeToString(operation), destkey, key);
		}

		public virtual long BitOp(BitOpType operation, string destkey, string key, params string[] keys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			return SendQueryExpectNumber(MakeCommand("BITOP", BitOpTypeToString(operation), destkey, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual long BitOp(BitOpType operation, string destkey, IEnumerable<string> keys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("BITOP", BitOpTypeToString(operation), destkey, ValidateKeys(keys, nameof(keys))));
		}

		public virtual long BitPos(string key, bool bit)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("BITPOS", key, bit ? "1" : "0");
		}

		public virtual long BitPos(string key, bool bit, long start)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("BITPOS", key, bit ? "1" : "0", start.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual long BitPos(string key, bool bit, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("BITPOS", key, bit ? "1" : "0", start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual long Decr(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("DECR", key);
		}

		public virtual long DecrBy(string key, long decrement)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("DECRBY", key, decrement.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual string Get(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("GET", key);
		}

		public virtual bool GetBit(string key, uint offset)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("GETBIT", key, offset.ToString(NumberFormatInfo.InvariantInfo)) != 0L;
		}

		public virtual string GetRange(string key, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectString("GETRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual string GetSet(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectString("GETSET", key, value);
		}

		public virtual long Incr(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("INCR", key);
		}

		public virtual long IncrBy(string key, long increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("INCRBY", key, increment.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual double IncrByFloat(string key, double increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return double.TryParse(
				SendQueryExpectString("INCRBYFLOAT", key, increment.ToString(NumberFormatInfo.InvariantInfo)),
				NumberStyles.Float,
				NumberFormatInfo.InvariantInfo,
				out double value
				) ? value : double.NaN;
		}

		public virtual IEnumerable<string> MGet(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return CreateEnumerable(SendQueryExpectMultiBulk("MGET", key));
		}

		public virtual IEnumerable<string> MGet(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			return CreateEnumerable(SendQueryExpectMultiBulk(MakeCommand("MGET", key, ValidateKeys(keys, nameof(keys)))));
		}

		public virtual IEnumerable<string> MGet(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return new string[0];
			return CreateEnumerable(SendQueryExpectMultiBulk(MakeCommand("MGET", ValidateKeys(keysArg, nameof(keys)))));
		}

		public virtual void MSet(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			SendQueryExpectOk("MSET", key, value);
		}

		public virtual void MSet(IEnumerable<KeyValuePair<string, string>> keyValueCollection)
		{
			if (keyValueCollection == null)
				throw new ArgumentNullException(nameof(keyValueCollection));

			var data = new List<string>();
			foreach (KeyValuePair<string, string> kvp in keyValueCollection)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keyValueCollection));
				if (kvp.Value == null)
					continue;
				data.Add(kvp.Key);
				data.Add(kvp.Value);
			}

			if (data.Count == 0)
				return;

			SendQueryExpectOk(MakeCommand("MSET", data));
		}

		public virtual void MSet(params string[] keysAndValues)
		{
			if (keysAndValues == null)
				throw new ArgumentNullException(nameof(keysAndValues));
			if (keysAndValues.Length == 0 || keysAndValues.Length % 2 != 0)
				throw new ArgumentOutOfRangeException(nameof(keysAndValues));
			
			SendQueryExpectOk(MakeCommand("MSET", ValidateKeys(keysAndValues, nameof(keysAndValues))));
		}

		public virtual bool MSetNX(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("MSETNX", key, value) != 0L;
		}

		public virtual bool MSetNX(IEnumerable<KeyValuePair<string, string>> keyValueCollection)
		{
			if (keyValueCollection == null)
				throw new ArgumentNullException(nameof(keyValueCollection));

			var data = new List<string>();
			foreach (KeyValuePair<string, string> kvp in keyValueCollection)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keyValueCollection));
				if (kvp.Value == null)
					continue;
				data.Add(kvp.Key);
				data.Add(kvp.Value);
			}

			if (data.Count == 0)
				return false;

			return SendQueryExpectNumber(MakeCommand("MSETNX", data)) != 0L;
		}

		public virtual bool MSetNX(params string[] keysAndValues)
		{
			if (keysAndValues == null)
				throw new ArgumentNullException(nameof(keysAndValues));
			if (keysAndValues.Length == 0 || keysAndValues.Length % 2 != 0)
				throw new ArgumentOutOfRangeException(nameof(keysAndValues));

			return SendQueryExpectNumber(MakeCommand("MSETNX", ValidateKeys(keysAndValues, nameof(keysAndValues)))) != 0L;
		}

		public virtual void PSetEx(string key, string value, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(milliseconds));
			SendQueryExpectOk("PSETEX", key, milliseconds.ToString(NumberFormatInfo.InvariantInfo), value); 
		}

		public virtual void Set(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			SendQueryExpectOk("SET", key, value);
		}

		public virtual bool Set(string key, string value, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return IsOk(SendQueryExpectStatement(MakeCommand("SET", key, value, mode == ExistenceMode.XX ? "XX" : "NX")));
		}

		public virtual void Set(string key, string value, TimeSpan expireTime)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			long milliseconds = (long)Math.Round(expireTime.TotalMilliseconds);
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException(nameof(expireTime));

			SendQueryExpectOk("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual void Set(string key, string value, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException(nameof(milliseconds));

			SendQueryExpectOk("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual bool Set(string key, string value, TimeSpan expireTime, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			long milliseconds = (long)Math.Round(expireTime.TotalMilliseconds);
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException("expireTime");

			return IsOk(SendQueryExpectStatement(MakeCommand("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo), mode == ExistenceMode.XX ? "XX" : "NX")));
		}

		public virtual bool Set(string key, string value, long milliseconds, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException(nameof(milliseconds));

			return IsOk(SendQueryExpectStatement(MakeCommand("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo), mode == ExistenceMode.XX ? "XX" : "NX")));
		}

		public virtual bool SetBit(string key, uint offset, bool value)
		{
			return SendQueryExpectNumber("SETBIT", key, offset.ToString(NumberFormatInfo.InvariantInfo), value ? "1" : "0") != 0L;
		}

		public virtual void SetEx(string key, string value, int seconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (seconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(seconds));
			SendQueryExpectOk("SETEX", key, seconds.ToString(NumberFormatInfo.InvariantInfo), value);
		}


		public virtual bool SetNx(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return 0L != SendQueryExpectNumber("SETNX", key, value);
		}

		public virtual long SetRange(string key, uint offset, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumber("SETRANGE", key, offset.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual long StrLen(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("STRLEN", key);
		}

	}
}
