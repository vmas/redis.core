﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Redis.Core
{
	sealed class RedisConnectionStringBuilder : DbConnectionStringBuilder
	{
		public static readonly int DefaultPoolSizeMultiplier = 10;
		public static readonly int DefaultPoolTimeout = 2000;

		public RedisEndPoint[] Servers
		{
			get
			{
				var endPoints = new HashSet<RedisEndPoint>();
				if (this.TryGetValue("Servers", out object obj))
				{
					var endPointsStrings = obj as string;
					if (endPointsStrings != null)
					{
						foreach (string endPointString in endPointsStrings.Split(','))
						{
							endPoints.Add(RedisEndPoint.Parse(endPointString));
						}
					}
				}
				return endPoints.ToArray();
			}
			set
			{
				if (value == null || value.Length == 0)
					this.Remove("Servers");
				else
					this["Servers"] = string.Join(",", value.Select(ep => ep.ToString()));
			}
		}

		public int Database
		{
			get
			{
				if (this.TryGetValue("Database", out object obj))
				{
					if (int.TryParse(obj as string, out int value))
						return value;
				}
				return RedisEndPoint.DefaultDatabase;
			}
			set
			{
				if (value <= 0)
					this.Remove("Database");
				else
					this["Database"] = value.ToString();
			}
		}

		public int PoolSizeMultiplier
		{
			get
			{
				if (this.TryGetValue("Pool Size Multiplier", out object obj))
				{
					if (int.TryParse(obj as string, out int value))
						return value;
				}
				return RedisConnectionStringBuilder.DefaultPoolSizeMultiplier;
			}
			set
			{
				if (value <= 0)
					this.Remove("Pool Size Multiplier");
				else
					this["Pool Size Multiplier"] = value.ToString();
			}
		}

		public int PoolTimeout
		{
			get
			{
				if (this.TryGetValue("Pool Timeout", out object obj))
				{
					if (int.TryParse(obj as string, out int value))
						return value;
				}
				return RedisConnectionStringBuilder.DefaultPoolTimeout;
			}
			set
			{
				if (value <= 0)
					this.Remove("Pool Timeout");
				else
					this["Pool Timeout"] = value.ToString();
			}
		}

	}

}