namespace Redis.Core
{
	public interface IRedisClientFactory
	{
		RedisClient CreateRedisClient(RedisEndPoint endPoint);
	}
}