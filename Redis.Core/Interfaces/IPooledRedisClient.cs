﻿using System.Threading.Tasks;

namespace Redis.Core.Interfaces
{
	public interface IPooledRedisClient
	{
		void ResetTransaction();
		Task ResetTransactionAsync();
		void AuthWithDefaultPassword();
		Task AuthWithDefaultPasswordAsync();
		void SelectDefaultDatabase();
		Task SelectDefaultDatabaseAsync();
	}
}
