﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RespService.DataTypes;

namespace Redis.Core.Interfaces
{
	internal interface ITransactionTask
	{
		object Execute(RespData response);
	}
}
