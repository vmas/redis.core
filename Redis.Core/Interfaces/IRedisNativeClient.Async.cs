﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Redis.Core.Interfaces
{
	public partial interface IRedisNativeClient
	{
		#region Keys

		Task<bool> DelAsync(string key);
		Task<long> DelAsync(string key, params string[] keys);
		Task<long> DelAsync(IEnumerable<string> keys);
		Task<string> DumpAsync(string key);
		Task<bool> ExistsAsync(string key);
		Task<long> ExistsAsync(string key, params string[] keys);
		Task<long> ExistsAsync(IEnumerable<string> keys);
		Task<bool> ExpireAsync(string key, long seconds);
		Task<bool> ExpireAtAsync(string key, long timestamp);
		Task<IEnumerable<string>> KeysAsync(string pattern);
		Task<bool> MigrateAsync(string host, int port, string key, int destinationDb, long timeoutMilliseconds, bool copy = false, bool replace = false, params string[] keys);
		Task<bool> MoveAsync(string key, int db);
		Task<string> ObjectAsync(ObjectSubcommands subcommand, params string[] arguments);
		Task<bool> PersistAsync(string key);
		Task<bool> PExpireAsync(string key, long milliseconds);
		Task<bool> PExpireAtAsync(string key, long millisecondsTimestamp);
		Task<long> PTTLAsync(string key);
		Task<string> RandomKeyAsync();
		Task RenameAsync(string key, string newkey);
		Task<bool> RenameNxAsync(string key, string newkey);
		Task<bool> RestoreAsync(string key, long ttl, string serializedValue, bool replace = false);
		Task<ScanResult<IEnumerable<string>>> ScanAsync(long cursor, string pattern = null, int? count = null);
		Task<ScanResult<IEnumerable<string>>> ScanAsync(string pattern = null, int? count = null);
		Task<IEnumerable<string>> SortAsync(string key);
		Task<IEnumerable<string>> SortAsync(string key, SortOptions sortOptions);
		Task<bool> TouchAsync(string key);
		Task<long> TouchAsync(string key, params string[] keys);
		Task<long> TouchAsync(IEnumerable<string> keys);
		Task<long> TTLAsync(string key);
		Task<string> TypeAsync(string key);
		Task<bool> UnlinkAsync(string key);
		Task<long> UnlinkAsync(string key, params string[] keys);
		Task<long> UnlinkAsync(IEnumerable<string> keys);
		Task<long> WaitAsync(int numslaves, long timeout);

		#endregion

		#region Hash operations

		Task<long> HDelAsync(string key, string field);
		Task<long> HDelAsync(string key, string field, params string[] fields);
		Task<long> HDelAsync(string key, IEnumerable<string> fields);
		Task<bool> HExistsAsync(string key, string field);
		Task<string> HGetAsync(string key, string field);
		Task<IEnumerable<KeyValuePair<string, string>>> HGetAllAsync(string key);
		Task<long> HIncrByAsync(string key, string field, long increment);
		Task<double> HIncrByFloatAsync(string key, string field, double increment);
		Task<IEnumerable<string>> HKeysAsync(string key);
		Task<long> HLenAsync(string key);
		Task<IEnumerable<string>> HMGetAsync(string key, params string[] fields);
		Task<IEnumerable<string>> HMGetAsync(string key, IEnumerable<string> fields);
		Task HMSetAsync(string key, IEnumerable<KeyValuePair<string, string>> fieldsWithValues);
		Task<ScanResult<IDictionary<string, string>>> HScanAsync(string key, long cursor, string pattern = null, int? count = null);
		Task<ScanResult<IDictionary<string, string>>> HScanAsync(string key, string pattern = null, int? count = null);
		Task<bool> HSetAsync(string key, string field, string value);
		Task<bool> HSetNXAsync(string key, string field, string value);
		Task<long> HStrLenAsync(string key, string field);
		Task<IEnumerable<string>> HValsAsync(string key);

		#endregion

		#region Strings

		Task<long> AppendAsync(string key, string value);
		Task<long> BitCountAsync(string key);
		Task<long> BitCountAsync(string key, long start, long end);
		// TODO: BITFIELD key [GET type offset] [SET type offset value] [INCRBY type offset increment] [OVERFLOW WRAP|SAT|FAIL]
		Task<long> BitOpAsync(BitOpType operation, string destkey, string key);
		Task<long> BitOpAsync(BitOpType operation, string destkey, string key, params string[] keys);
		Task<long> BitOpAsync(BitOpType operation, string destkey, IEnumerable<string> keys);
		Task<long> BitPosAsync(string key, bool bit);
		Task<long> BitPosAsync(string key, bool bit, long start);
		Task<long> BitPosAsync(string key, bool bit, long start, long end);
		Task<long> DecrAsync(string key);
		Task<long> DecrByAsync(string key, long decrement);
		Task<string> GetAsync(string key);
		Task<bool> GetBitAsync(string key, uint offset);
		Task<string> GetRangeAsync(string key, long start, long end);
		Task<string> GetSetAsync(string key, string value);
		Task<long> IncrAsync(string key);
		Task<long> IncrByAsync(string key, long increment);
		Task<double> IncrByFloatAsync(string key, double increment);
		Task<IEnumerable<string>> MGetAsync(string key);
		Task<IEnumerable<string>> MGetAsync(string key, params string[] keys);
		Task<IEnumerable<string>> MGetAsync(IEnumerable<string> keys);
		Task MSetAsync(string key, string value);
		Task MSetAsync(IEnumerable<KeyValuePair<string, string>> keyValueCollection);
		Task MSetAsync(params string[] keysAndValues);
		Task<bool> MSetNXAsync(string key, string value);
		Task<bool> MSetNXAsync(IEnumerable<KeyValuePair<string, string>> keyValueCollection);
		Task<bool> MSetNXAsync(params string[] keysAndValues);
		Task PSetExAsync(string key, string value, long milliseconds);
		Task SetAsync(string key, string value);
		Task<bool> SetAsync(string key, string value, ExistenceMode mode);
		Task SetAsync(string key, string value, TimeSpan expireTime);
		Task SetAsync(string key, string value, long milliseconds);
		Task<bool> SetAsync(string key, string value, TimeSpan expireTime, ExistenceMode mode);
		Task<bool> SetAsync(string key, string value, long milliseconds, ExistenceMode mode);
		Task<bool> SetBitAsync(string key, uint offset, bool value);
		Task SetExAsync(string key, string value, int seconds);
		Task<bool> SetNxAsync(string key, string value);
		Task<long> SetRangeAsync(string key, uint offset, string value);
		Task<long> StrLenAsync(string key);

		#endregion


		#region Sorted Set operations

		Task<long> ZAddAsync(string key, double score, string member);
		Task<long> ZAddAsync(string key, double defaultScore, params string[] members);
		Task<string> ZAddAsync(string key, ExistenceMode mode, bool returnChanged, bool incr, double score, string member);
		Task<long> ZAddAsync(string key, ExistenceMode mode, bool returnChanged, IEnumerable<KeyValuePair<string, double>> valuesWithScores);
		Task<long> ZCardAsync(string key);
		Task<long> ZCountAsync(string key, double min, double max);
		Task<long> ZCountAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<double> ZIncrByAsync(string key, double increment, string member);
		Task<long> ZInterStoreAsync(string destination, UnionAggregate aggregate, params string[] keys);
		Task<long> ZInterStoreAsync(string destination, IEnumerable<string> keys, UnionAggregate aggregate);
		Task<long> ZInterStoreAsync(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate);
		Task<long> ZLexCountAsync(string key, string min, string max);
		Task<ICollection<string>> ZRangeAsync(string key, long start, long stop);
		Task<ICollection<KeyValuePair<string, double>>> ZRangeWithScoresAsync(string key, long start, long stop);
		Task<ICollection<string>> ZRangeByLexAsync(string key, string min, string max);
		Task<ICollection<string>> ZRangeByLexAsync(string key, string min, string max, long offset, long count);
		Task<ICollection<string>> ZRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<ICollection<string>> ZRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		Task<ICollection<KeyValuePair<string, double>>> ZRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<ICollection<KeyValuePair<string, double>>> ZRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		Task<long?> ZRankAsync(string key, string member);
		Task<long> ZRemAsync(string key, string member);
		Task<long> ZRemAsync(string key, string member, params string[] members);
		Task<long> ZRemAsync(string key, IEnumerable<string> members);
		Task<long> ZRemRangeByLexAsync(string key, string min, string max);
		Task<long> ZRemRangeByRankAsync(string key, long start, long stop);
		Task<long> ZRemRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<ICollection<string>> ZRevRangeAsync(string key, long start, long stop);
		Task<ICollection<KeyValuePair<string, double>>> ZRevRangeWithScoresAsync(string key, long start, long stop);
		Task<ICollection<string>> ZRevRangeByLexAsync(string key, string min, string max);
		Task<ICollection<string>> ZRevRangeByLexAsync(string key, string min, string max, long offset, long count);
		Task<ICollection<string>> ZRevRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<ICollection<string>> ZRevRangeByScoreAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		Task<ICollection<KeyValuePair<string, double>>> ZRevRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax);
		Task<ICollection<KeyValuePair<string, double>>> ZRevRangeByScoreWithScoresAsync(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		Task<long?> ZRevRankAsync(string key, string member);
		Task<ScanResult<ICollection<KeyValuePair<string, double>>>> ZScanAsync(string key, long cursor, string pattern = null, int? count = null);
		Task<ScanResult<ICollection<KeyValuePair<string, double>>>> ZScanAsync(string key, string pattern = null, int? count = null);
		Task<double?> ZScoreAsync(string key, string member);
		Task<long> ZUnionStoreAsync(string destination, UnionAggregate aggregate, params string[] keys);
		Task<long> ZUnionStoreAsync(string destination, IEnumerable<string> keys, UnionAggregate aggregate);
		Task<long> ZUnionStoreAsync(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate);

		#endregion

		#region Set operations

		Task<bool> SAddAsync(string key, string member);
		Task<long> SAddAsync(string key, string member, params string[] members);
		Task<long> SAddAsync(string key, IEnumerable<string> members);
		Task<long> SCardAsync(string key);
		Task<ICollection<string>> SDiffAsync(string key);
		Task<ICollection<string>> SDiffAsync(string key, params string[] keys);
		Task<ICollection<string>> SDiffAsync(IEnumerable<string> keys);
		Task<long> SDiffStoreAsync(string destination, string key);
		Task<long> SDiffStoreAsync(string destination, string key, params string[] keys);
		Task<long> SDiffStoreAsync(string destination, IEnumerable<string> keys);
		Task<ICollection<string>> SInterAsync(string key);
		Task<ICollection<string>> SInterAsync(string key, params string[] keys);
		Task<ICollection<string>> SInterAsync(IEnumerable<string> keys);
		Task<long> SInterStoreAsync(string destination, string key);
		Task<long> SInterStoreAsync(string destination, string key, params string[] keys);
		Task<long> SInterStoreAsync(string destination, IEnumerable<string> keys);
		Task<bool> SIsMemberAsync(string key, string member);
		Task<ICollection<string>> SMembersAsync(string key);
		Task<bool> SMoveAsync(string source, string destination, string member);
		Task<string> SPopAsync(string key);
		Task<ICollection<string>> SPopAsync(string key, int count);
		Task<string> SRandMemberAsync(string key);
		Task<ICollection<string>> SRandMemberAsync(string key, int count);
		Task<long> SRemAsync(string key, string member);
		Task<long> SRemAsync(string key, string member, params string[] members);
		Task<long> SRemAsync(string key, IEnumerable<string> members);
		Task<ScanResult<ICollection<string>>> SScanAsync(string key, long cursor, string pattern = null, int? count = null);
		Task<ScanResult<ICollection<string>>> SScanAsync(string key, string pattern = null, int? count = null);
		Task<ICollection<string>> SUnionAsync(string key);
		Task<ICollection<string>> SUnionAsync(string key, params string[] keys);
		Task<ICollection<string>> SUnionAsync(IEnumerable<string> keys);
		Task<long> SUnionStoreAsync(string destination, string key);
		Task<long> SUnionStoreAsync(string destination, string key, params string[] keys);
		Task<long> SUnionStoreAsync(string destination, IEnumerable<string> keys);

		#endregion

		#region Connections

		Task<string> EchoAsync(string message);
		Task<string> PingAsync();
		Task<string> PingAsync(string message);
		Task SwapDBAsync(int a, int b);

		#endregion

		#region Lists

		Task<KeyValuePair<string, string>?> BLPopAsync(string key, long timeout);
		Task<KeyValuePair<string, string>?> BLPopAsync(long timeout, string key, params string[] keys);
		Task<KeyValuePair<string, string>?> BLPopAsync(IEnumerable<string> keys, long timeout);
		Task<KeyValuePair<string, string>?> BRPopAsync(string key, long timeout);
		Task<KeyValuePair<string, string>?> BRPopAsync(long timeout, string key, params string[] keys);
		Task<KeyValuePair<string, string>?> BRPopAsync(IEnumerable<string> keys, long timeout);
		Task<string> BRPopLPushAsync(string source, string destination, long timeout);
		Task<string> LIndexAsync(string key, long index);
		Task<long> LInsertAsync(string key, bool before, string pivot, string value);
		Task<long> LLenAsync(string key);
		Task<string> LPopAsync(string key);
		Task<long> LPushAsync(string key, string value);
		Task<long> LPushAsync(string key, string value, params string[] values);
		Task<long> LPushAsync(string key, IEnumerable<string> values);
		Task<long> LPushXAsync(string key, string value);
		Task<IList<string>> LRangeAsync(string key, long start, long stop);
		Task<long> LRemAsync(string key, long count, string value);
		Task LSetAsync(string key, long index, string value);
		Task LTrimAsync(string key, long start, long stop);
		Task<string> RPopAsync(string key);
		Task<string> RPopLPushAsync(string source, string destination);
		Task<long> RPushAsync(string key, string value);
		Task<long> RPushAsync(string key, string value, params string[] values);
		Task<long> RPushAsync(string key, IEnumerable<string> values);
		Task<long> RPushXAsync(string key, string value);

		#endregion Lists

		#region Geo

		Task<bool> GeoAddAsync(string key, double longitude, double latitude, string member);
		Task<bool> GeoAddAsync(string key, GeoObject geo);
		Task<long> GeoAddAsync(string key, IEnumerable<GeoObject> geo);
		Task<string> GeoHashAsync(string key, string member);
		Task<string[]> GeoHashAsync(string key, string[] members);
		Task<GeoObject?> GeoPosAsync(string key, string member);
		Task<IEnumerable<GeoObject>> GeoPosAsync(string key, string member, params string[] members);
		Task<IEnumerable<GeoObject>> GeoPosAsync(string key, IEnumerable<string> members);
		Task<double?> GeoDistAsync(string key, string member1, string member2);
		Task<double?> GeoDistAsync(string key, string member1, string member2, GeoUnits unit);
		Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusReadOnlyAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, string storeKey, string storeDistKey);
		Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey);
		Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey);
		Task<long> GeoRadiusAsync(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		Task<IEnumerable<GeoData>> GeoRadiusByMemberReadOnlyAsync(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, string storeKey, string storeDistKey);
		Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey);
		Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey);
		Task<long> GeoRadiusByMemberAsync(string key, string member, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey);

		#endregion

		#region HyperLogLog

		Task<bool> PfAddAsync(string key, string element);
		Task<bool> PfAddAsync(string key, string element, params string[] elements);
		Task<bool> PfAddAsync(string key, IEnumerable<string> elements);
		Task<long> PfCountAsync(string key);
		Task<long> PfCountAsync(string key, params string[] keys);
		Task PfMergeAsync(string destkey, string sourcekey);
		Task PfMergeAsync(string destkey, string sourcekey, params string[] sourcekeys);
		Task PfMergeAsync(string destkey, IEnumerable<string> sourcekeys);

		#endregion HyperLogLog

		#region Transactions

		Task DiscardAsync();
		Task<object[]> ExecAsync();
		Task MultiAsync();


		#endregion
	}
}
