﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Redis.Core.Interfaces
{
	public partial interface IRedisNativeClient : IDisposable
	{

		int Database { get; }


		#region Keys

		bool Del(string key);
		long Del(string key, params string[] keys);
		long Del(IEnumerable<string> keys);
		string Dump(string key);
		bool Exists(string key);
		long Exists(string key, params string[] keys);
		long Exists(IEnumerable<string> keys);
		bool Expire(string key, long seconds);
		bool ExpireAt(string key, long timestamp);
		IEnumerable<string> Keys(string pattern);
		bool Migrate(string host, int port, string key, int destinationDb, long timeoutMilliseconds, bool copy = false, bool replace = false, params string[] keys);
		bool Move(string key, int db);
		string Object(ObjectSubcommands subcommand, params string[] arguments);
		bool Persist(string key);
		bool PExpire(string key, long milliseconds);
		bool PExpireAt(string key, long millisecondsTimestamp);
		long PTTL(string key);
		string RandomKey();
		void Rename(string key, string newkey);
		bool RenameNx(string key, string newkey);
		bool Restore(string key, long ttl, string serializedValue, bool replace = false);
		ScanResult<IEnumerable<string>> Scan(long cursor, string pattern = null, int? count = null);
		ScanResult<IEnumerable<string>> Scan(string pattern = null, int? count = null);
		IEnumerable<string> Sort(string key);
		IEnumerable<string> Sort(string key, SortOptions sortOptions);
		bool Touch(string key);
		long Touch(string key, params string[] keys);
		long Touch(IEnumerable<string> keys);
		long TTL(string key);
		string Type(string key);
		bool Unlink(string key);
		long Unlink(string key, params string[] keys);
		long Unlink(IEnumerable<string> keys);
		long Wait(int numslaves, long timeout);

		#endregion

		#region Hash operations

		long HDel(string key, string field);
		long HDel(string key, string field, params string[] fields);
		long HDel(string key, IEnumerable<string> fields);
		bool HExists(string key, string field);
		string HGet(string key, string field);
		IEnumerable<KeyValuePair<string, string>> HGetAll(string key);
		long HIncrBy(string key, string field, long increment);
		double HIncrByFloat(string key, string field, double increment);
		IEnumerable<string> HKeys(string key);
		long HLen(string key);
		IEnumerable<string> HMGet(string key, params string[] fields);
		IEnumerable<string> HMGet(string key, IEnumerable<string> fields);
		void HMSet(string key, IEnumerable<KeyValuePair<string, string>> fieldsWithValues);
		ScanResult<IDictionary<string, string>> HScan(string key, long cursor, string pattern = null, int? count = null);
		ScanResult<IDictionary<string, string>> HScan(string key, string pattern = null, int? count = null);
		bool HSet(string key, string field, string value);
		bool HSetNX(string key, string field, string value);
		long HStrLen(string key, string field);
		IEnumerable<string> HVals(string key);

		#endregion

		#region Strings

		long Append(string key, string value);
		long BitCount(string key);
		long BitCount(string key, long start, long end);
		//// TODO: BITFIELD key [GET type offset] [SET type offset value] [INCRBY type offset increment] [OVERFLOW WRAP|SAT|FAIL]
		long BitOp(BitOpType operation, string destkey, string key);
		long BitOp(BitOpType operation, string destkey, string key, params string[] keys);
		long BitOp(BitOpType operation, string destkey, IEnumerable<string> keys);
		long BitPos(string key, bool bit);
		long BitPos(string key, bool bit, long start);
		long BitPos(string key, bool bit, long start, long end);
		long Decr(string key);
		long DecrBy(string key, long decrement);
		string Get(string key);
		bool GetBit(string key, uint offset);
		string GetRange(string key, long start, long end);
		string GetSet(string key, string value);
		long Incr(string key);
		long IncrBy(string key, long increment);
		double IncrByFloat(string key, double increment);
		IEnumerable<string> MGet(string key);
		IEnumerable<string> MGet(string key, params string[] keys);
		IEnumerable<string> MGet(IEnumerable<string> keys);
		void MSet(string key, string value);
		void MSet(IEnumerable<KeyValuePair<string, string>> keyValueCollection);
		void MSet(params string[] keysAndValues);
		bool MSetNX(string key, string value);
		bool MSetNX(IEnumerable<KeyValuePair<string, string>> keyValueCollection);
		bool MSetNX(params string[] keysAndValues);
		void PSetEx(string key, string value, long milliseconds);
		void Set(string key, string value);
		bool Set(string key, string value, ExistenceMode mode);
		void Set(string key, string value, TimeSpan expireTime);
		void Set(string key, string value, long milliseconds);
		bool Set(string key, string value, TimeSpan expireTime, ExistenceMode mode);
		bool Set(string key, string value, long milliseconds, ExistenceMode mode);
		bool SetBit(string key, uint offset, bool value);
		void SetEx(string key, string value, int seconds);
		bool SetNx(string key, string value);
		long SetRange(string key, uint offset, string value);
		long StrLen(string key);

		#endregion

		#region Set operations

		bool SAdd(string key, string member);
		long SAdd(string key, string member, params string[] members);
		long SAdd(string key, IEnumerable<string> members);
		long SCard(string key);
		ICollection<string> SDiff(string key);
		ICollection<string> SDiff(string key, params string[] keys);
		ICollection<string> SDiff(IEnumerable<string> keys);
		long SDiffStore(string destination, string key);
		long SDiffStore(string destination, string key, params string[] keys);
		long SDiffStore(string destination, IEnumerable<string> keys);
		ICollection<string> SInter(string key);
		ICollection<string> SInter(string key, params string[] keys);
		ICollection<string> SInter(IEnumerable<string> keys);
		long SInterStore(string destination, string key);
		long SInterStore(string destination, string key, params string[] keys);
		long SInterStore(string destination, IEnumerable<string> keys);
		bool SIsMember(string key, string member);
		ICollection<string> SMembers(string key);
		bool SMove(string source, string destination, string member);
		string SPop(string key);
		ICollection<string> SPop(string key, int count);
		string SRandMember(string key);
		ICollection<string> SRandMember(string key, int count);
		long SRem(string key, string member);
		long SRem(string key, string member, params string[] members);
		long SRem(string key, IEnumerable<string> members);
		ScanResult<ICollection<string>> SScan(string key, long cursor, string pattern = null, int? count = null);
		ScanResult<ICollection<string>> SScan(string key, string pattern = null, int? count = null);
		ICollection<string> SUnion(string key);
		ICollection<string> SUnion(string key, params string[] keys);
		ICollection<string> SUnion(IEnumerable<string> keys);
		long SUnionStore(string destination, string key);
		long SUnionStore(string destination, string key, params string[] keys);
		long SUnionStore(string destination, IEnumerable<string> keys);

		#endregion

		#region Sorted Set operations

		//TODO: BZPOPMAX key [key ...] timeout
		//TODO: BZPOPMIN key [key ...] timeout
		long ZAdd(string key, double score, string member);
		long ZAdd(string key, double defaultScore, params string[] members);
		string ZAdd(string key, ExistenceMode mode, bool returnChanged, bool incr, double score, string member);
		long ZAdd(string key, ExistenceMode mode, bool returnChanged, IEnumerable<KeyValuePair<string, double>> valuesWithScores);
		long ZCard(string key);
		long ZCount(string key, double min, double max);
		long ZCount(string key, double min, bool excludeMin, double max, bool excludeMax);
		double ZIncrBy(string key, double increment, string member);
		long ZInterStore(string destination, UnionAggregate aggregate, params string[] keys);
		long ZInterStore(string destination, IEnumerable<string> keys, UnionAggregate aggregate);
		long ZInterStore(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate);
		long ZLexCount(string key, string min, string max);
		//TODO: ZPOPMAX key [key ...] timeout
		//TODO: ZPOPMIN key [key ...] timeout
		ICollection<string> ZRange(string key, long start, long stop);
		ICollection<KeyValuePair<string, double>> ZRangeWithScores(string key, long start, long stop);
		ICollection<string> ZRangeByLex(string key, string min, string max);
		ICollection<string> ZRangeByLex(string key, string min, string max, long offset, long count);
		ICollection<string> ZRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax);
		ICollection<string> ZRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		ICollection<KeyValuePair<string, double>> ZRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax);
		ICollection<KeyValuePair<string, double>> ZRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		long? ZRank(string key, string member);
		long ZRem(string key, string member);
		long ZRem(string key, string member, params string[] members);
		long ZRem(string key, IEnumerable<string> members);
		long ZRemRangeByLex(string key, string min, string max);
		long ZRemRangeByRank(string key, long start, long stop);
		long ZRemRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax);
		ICollection<string> ZRevRange(string key, long start, long stop);
		ICollection<KeyValuePair<string, double>> ZRevRangeWithScores(string key, long start, long stop);
		ICollection<string> ZRevRangeByLex(string key, string min, string max);
		ICollection<string> ZRevRangeByLex(string key, string min, string max, long offset, long count);
		ICollection<string> ZRevRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax);
		ICollection<string> ZRevRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		ICollection<KeyValuePair<string, double>> ZRevRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax);
		ICollection<KeyValuePair<string, double>> ZRevRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count);
		long? ZRevRank(string key, string member);
		ScanResult<ICollection<KeyValuePair<string, double>>> ZScan(string key, long cursor, string pattern = null, int? count = null);
		ScanResult<ICollection<KeyValuePair<string, double>>> ZScan(string key, string pattern = null, int? count = null);
		double? ZScore(string key, string member);
		long ZUnionStore(string destination, UnionAggregate aggregate, params string[] keys);
		long ZUnionStore(string destination, IEnumerable<string> keys, UnionAggregate aggregate);
		long ZUnionStore(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate);

		#endregion

		#region Connections

		string Echo(string message);
		string Ping();
		string Ping(string message);
		void SwapDB(int a, int b);

		#endregion

		#region Lists

		KeyValuePair<string, string>? BLPop(string key, long timeout);
		KeyValuePair<string, string>? BLPop(long timeout, string key, params string[] keys);
		KeyValuePair<string, string>? BLPop(IEnumerable<string> keys, long timeout);
		KeyValuePair<string, string>? BRPop(string key, long timeout);
		KeyValuePair<string, string>? BRPop(long timeout, string key, params string[] keys);
		KeyValuePair<string, string>? BRPop(IEnumerable<string> keys, long timeout);
		string BRPopLPush(string source, string destination, long timeout);
		string LIndex(string key, long index);
		long LInsert(string key, bool before, string pivot, string value);
		long LLen(string key);
		string LPop(string key);
		long LPush(string key, string value);
		long LPush(string key, string value, params string[] values);
		long LPush(string key, IEnumerable<string> values);
		long LPushX(string key, string value);
		IList<string> LRange(string key, long start, long stop);
		long LRem(string key, long count, string value);
		void LSet(string key, long index, string value);
		void LTrim(string key, long start, long stop);
		string RPop(string key);
		string RPopLPush(string source, string destination);
		long RPush(string key, string value);
		long RPush(string key, string value, params string[] values);
		long RPush(string key, IEnumerable<string> values);
		long RPushX(string key, string value);

		#endregion Lists

		#region Geo

		bool GeoAdd(string key, double longitude, double latitude, string member);
		bool GeoAdd(string key, GeoObject geo);
		long GeoAdd(string key, IEnumerable<GeoObject> geo);
		string GeoHash(string key, string member);
		string[] GeoHash(string key, string[] members);
		GeoObject? GeoPos(string key, string member);
		IEnumerable<GeoObject> GeoPos(string key, string member, params string[] members);
		IEnumerable<GeoObject> GeoPos(string key, IEnumerable<string> members);
		double? GeoDist(string key, string member1, string member2);
		double? GeoDist(string key, string member1, string member2, GeoUnits unit);
		IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		IEnumerable<GeoData> GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		IEnumerable<GeoData> GeoRadiusReadOnly(string key, double longtitude, double latitude, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, string storeKey, string storeDistKey);
		long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey);
		long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey);
		long GeoRadius(string key, double longtitude, double latitude, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey);
		IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		IEnumerable<GeoData> GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash);
		IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count);
		IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, ListSortDirection order);
		IEnumerable<GeoData> GeoRadiusByMemberReadOnly(string key, string member, double radius, GeoUnits unit, bool withCoord, bool withDist, bool withHash, int count, ListSortDirection order);
		long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, string storeKey, string storeDistKey);
		long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, int count, string storeKey, string storeDistKey);
		long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, ListSortDirection order, string storeKey, string storeDistKey);
		long GeoRadiusByMember(string key, string member, double radius, GeoUnits unit, int count, ListSortDirection order, string storeKey, string storeDistKey);

		#endregion

		#region HyperLogLog

		bool PfAdd(string key, string element);
		bool PfAdd(string key, string element, params string[] elements);
		bool PfAdd(string key, IEnumerable<string> elements);
		long PfCount(string key);
		long PfCount(string key, params string[] keys);
		void PfMerge(string destkey, string sourcekey);
		void PfMerge(string destkey, string sourcekey, params string[] sourcekeys);
		void PfMerge(string destkey, IEnumerable<string> sourcekeys);

		#endregion HyperLogLog
	}
}
