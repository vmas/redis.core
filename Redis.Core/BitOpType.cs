﻿namespace Redis.Core
{
	public enum BitOpType
	{
		And,
		Or,
		Xor,
		Not
	}
}
