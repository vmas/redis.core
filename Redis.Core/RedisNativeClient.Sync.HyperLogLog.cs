﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual bool PfAdd(string key, string element)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (element == null)
				throw new ArgumentNullException(nameof(element));
			return 0L != SendQueryExpectNumber("PFADD", key, element);
		}

		public virtual bool PfAdd(string key, string element, params string[] elements)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (element == null)
				throw new ArgumentNullException(nameof(element));
			if (elements == null)
				throw new ArgumentNullException(nameof(elements));
			return 0L != SendQueryExpectNumber(MakeCommand("PFADD", key, element, ValidateKeys(elements, nameof(elements))));
		}

		public virtual bool PfAdd(string key, IEnumerable<string> elements)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (elements == null)
				throw new ArgumentNullException(nameof(elements));
			IList<string> elementsArg = elements as IList<string> ?? elements.ToArray();
			return 0L != SendQueryExpectNumber(MakeCommand("PFADD", key, ValidateKeys(elementsArg, nameof(elements))));
		}

		public virtual long PfCount(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("PFCOUNT", key);
		}

		public virtual long PfCount(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumber(MakeCommand("PFCOUNT", key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual void PfMerge(string destkey, string sourcekey)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekey == null)
				throw new ArgumentNullException(nameof(sourcekey));
			SendQueryExpectOk("PFMERGE", destkey, sourcekey);
		}

		public virtual void PfMerge(string destkey, string sourcekey, params string[] sourcekeys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekey == null)
				throw new ArgumentNullException(nameof(sourcekey));
			if (sourcekeys == null)
				throw new ArgumentNullException(nameof(sourcekeys));
			SendQueryExpectOk(MakeCommand("PFMERGE", destkey, sourcekey, ValidateKeys(sourcekeys, nameof(sourcekeys))));
		}

		public virtual void PfMerge(string destkey, IEnumerable<string> sourcekeys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (sourcekeys == null)
				throw new ArgumentNullException(nameof(sourcekeys));
			IList<string> sourcekeysArg = sourcekeys as IList<string> ?? sourcekeys.ToArray();
			SendQueryExpectOk(MakeCommand("PFMERGE", destkey, ValidateKeys(sourcekeysArg, nameof(sourcekeys))));
		}

	}
}
