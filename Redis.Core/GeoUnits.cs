﻿namespace Redis.Core
{
	public enum GeoUnits
	{
		Meters,
		Kilometers,
		Miles,
		Feet,
	}
}