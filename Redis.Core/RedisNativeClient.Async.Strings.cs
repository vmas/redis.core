﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual Task<long> AppendAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("APPEND", key, value);
		}

		public virtual Task<long> BitCountAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("BITCOUNT", key);
		}

		public virtual Task<long> BitCountAsync(string key, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("BITCOUNT", key, start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		//// TODO: BITFIELD key [GET type offset] [SET type offset value] [INCRBY type offset increment] [OVERFLOW WRAP|SAT|FAIL]

		public virtual Task<long> BitOpAsync(BitOpType operation, string destkey, string key)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumberAsync("BITOP", BitOpTypeToString(operation), destkey, key);
		}

		public virtual Task<long> BitOpAsync(BitOpType operation, string destkey, string key, params string[] keys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			return SendQueryExpectNumberAsync(MakeCommand("BITOP", BitOpTypeToString(operation), destkey, key, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task<long> BitOpAsync(BitOpType operation, string destkey, IEnumerable<string> keys)
		{
			if (destkey == null)
				throw new ArgumentNullException(nameof(destkey));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return SendQueryExpectNumberAsync(MakeCommand("BITOP", BitOpTypeToString(operation), destkey, ValidateKeys(keys, nameof(keys))));
		}

		public virtual Task<long> BitPosAsync(string key, bool bit)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("BITPOS", key, bit ? "1" : "0");
		}

		public virtual Task<long> BitPosAsync(string key, bool bit, long start)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("BITPOS", key, bit ? "1" : "0", start.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<long> BitPosAsync(string key, bool bit, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("BITPOS", key, bit ? "1" : "0", start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<long> DecrAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("DECR", key);
		}

		public virtual Task<long> DecrByAsync(string key, long decrement)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("DECRBY", key, decrement.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<string> GetAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("GET", key);
		}

		public virtual Task<bool> GetBitAsync(string key, uint offset)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectBoolAsync("GETBIT", key, offset.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<string> GetRangeAsync(string key, long start, long end)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectStringAsync("GETRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), end.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task<string> GetSetAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectStringAsync("GETSET", key, value);
		}

		public virtual Task<long> IncrAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("INCR", key);
		}

		public virtual Task<long> IncrByAsync(string key, long increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("INCRBY", key, increment.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual async Task<double> IncrByFloatAsync(string key, double increment)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			string result = await SendQueryExpectStringAsync("INCRBYFLOAT", key, increment.ToString(NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			return double.TryParse(result, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out double value) ? value : double.NaN;
		}

		public virtual async Task<IEnumerable<string>> MGetAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync("MGET", key).ConfigureAwait(false);
			if (response.HasValue)
				return CreateEnumerable(response.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual async Task<IEnumerable<string>> MGetAsync(string key, params string[] keys)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("MGET", key, ValidateKeys(keys, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateEnumerable(response.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual async Task<IEnumerable<string>> MGetAsync(IEnumerable<string> keys)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			string[] keysArg = keys.ToArray();
			if (keysArg.Length == 0)
				return new string[0];

			MultiBulk? response = await SendQueryExpectMultiBulkAsync(MakeCommand("MGET", ValidateKeys(keysArg, nameof(keys)))).ConfigureAwait(false);
			if (response.HasValue)
				return CreateEnumerable(response.Value);
			CreateTransactionTask<MultiBulk, IEnumerable<string>>(CreateEnumerable);
			return null;
		}

		public virtual Task MSetAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectOkAsync("MSET", key, value);
		}

		public virtual Task MSetAsync(IEnumerable<KeyValuePair<string, string>> keyValueCollection)
		{
			if (keyValueCollection == null)
				throw new ArgumentNullException(nameof(keyValueCollection));

			var data = new List<string>();
			foreach (KeyValuePair<string, string> kvp in keyValueCollection)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keyValueCollection));
				if (kvp.Value == null)
					continue;
				data.Add(kvp.Key);
				data.Add(kvp.Value);
			}

			if (data.Count == 0)
				return DefaultLongTask;

			return SendQueryExpectOkAsync(MakeCommand("MSET", data));
		}

		public virtual Task MSetAsync(params string[] keysAndValues)
		{
			if (keysAndValues == null)
				throw new ArgumentNullException(nameof(keysAndValues));
			if (keysAndValues.Length == 0 || keysAndValues.Length % 2 != 0)
				throw new ArgumentOutOfRangeException(nameof(keysAndValues));

			return SendQueryExpectOkAsync(MakeCommand("MSET", ValidateKeys(keysAndValues, nameof(keysAndValues))));
		}

		public virtual Task<bool> MSetNXAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectBoolAsync("MSETNX", key, value);
		}

		public virtual Task<bool> MSetNXAsync(IEnumerable<KeyValuePair<string, string>> keyValueCollection)
		{
			if (keyValueCollection == null)
				throw new ArgumentNullException(nameof(keyValueCollection));

			var data = new List<string>();
			foreach (KeyValuePair<string, string> kvp in keyValueCollection)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keyValueCollection));
				if (kvp.Value == null)
					continue;
				data.Add(kvp.Key);
				data.Add(kvp.Value);
			}

			if (data.Count == 0)
				return Task.FromResult(false);

			return SendQueryExpectBoolAsync(MakeCommand("MSETNX", data));
		}

		public virtual Task<bool> MSetNXAsync(params string[] keysAndValues)
		{
			if (keysAndValues == null)
				throw new ArgumentNullException(nameof(keysAndValues));
			if (keysAndValues.Length == 0 || keysAndValues.Length % 2 != 0)
				throw new ArgumentOutOfRangeException(nameof(keysAndValues));

			return SendQueryExpectBoolAsync(MakeCommand("MSETNX", ValidateKeys(keysAndValues, nameof(keysAndValues))));
		}

		public virtual Task PSetExAsync(string key, string value, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(milliseconds));
			return SendQueryExpectOkAsync("PSETEX", key, milliseconds.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task SetAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return SendQueryExpectOkAsync("SET", key, value);
		}

		public virtual async Task<bool> SetAsync(string key, string value, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return IsOk(await SendQueryExpectStatementAsync(MakeCommand("SET", key, value, mode == ExistenceMode.XX ? "XX" : "NX")).ConfigureAwait(false));
		}

		public virtual Task SetAsync(string key, string value, TimeSpan expireTime)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			long milliseconds = (long)Math.Round(expireTime.TotalMilliseconds);
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException(nameof(expireTime));

			return SendQueryExpectOkAsync("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual Task SetAsync(string key, string value, long milliseconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException("milliseconds");

			return SendQueryExpectOkAsync("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual async Task<bool> SetAsync(string key, string value, TimeSpan expireTime, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			long milliseconds = (long)Math.Round(expireTime.TotalMilliseconds);
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException("expireTime");

			return IsOk(await SendQueryExpectStatementAsync(MakeCommand("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo), mode == ExistenceMode.XX ? "XX" : "NX")).ConfigureAwait(false));
		}

		public virtual async Task<bool> SetAsync(string key, string value, long milliseconds, ExistenceMode mode)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (milliseconds <= 0L)
				throw new ArgumentOutOfRangeException("milliseconds");

			return IsOk(await SendQueryExpectStatementAsync(MakeCommand("SET", key, value, "PX", milliseconds.ToString(NumberFormatInfo.InvariantInfo), mode == ExistenceMode.XX ? "XX" : "NX")).ConfigureAwait(false));
		}

		public virtual Task<bool> SetBitAsync(string key, uint offset, bool value)
		{
			return SendQueryExpectBoolAsync("SETBIT", key, offset.ToString(NumberFormatInfo.InvariantInfo), value ? "1" : "0");
		}

		public virtual Task SetExAsync(string key, string value, int seconds)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			if (seconds <= 0)
				throw new ArgumentOutOfRangeException(nameof(seconds));
			return SendQueryExpectOkAsync("SETEX", key, seconds.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task<bool> SetNxAsync(string key, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));

			return SendQueryExpectBoolAsync("SETNX", key, value);
		}

		public virtual Task<long> SetRangeAsync(string key, uint offset, string value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (value == null)
				throw new ArgumentNullException(nameof(value));
			return SendQueryExpectNumberAsync("SETRANGE", key, offset.ToString(NumberFormatInfo.InvariantInfo), value);
		}

		public virtual Task<long> StrLenAsync(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumberAsync("STRLEN", key);
		}

	}
}