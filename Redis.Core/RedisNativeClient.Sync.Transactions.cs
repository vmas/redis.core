﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Redis.Core.Interfaces;
using RespService.DataTypes;
using Redis.Core.TransactionTasks;
using System.Threading;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		//private void CreateTransactionTask<T>()
		//{
		//	var tt = new TransactionTask();
		//	_transactionTasks.Enqueue(tt);
		//	return tt.Task;
		//}

		private void CreateInt64TransactionTask(string commandName)
		{
			_transactionTasks.Enqueue(new Int64TransactionTask(commandName));
		}

		private void CreateBoolTransactionTask(string commandName)
		{
			_transactionTasks.Enqueue(new BooleanTransactionTask(commandName));
		}

		private void CreateVoidTransactionTask(string commandName)
		{
			_transactionTasks.Enqueue(new VoidTransactionTask(commandName, this.Encoding));
		}

		private void CreateStringTransactionTask(string commandName)
		{
			_transactionTasks.Enqueue(new StringTransactionTask(commandName, this.Encoding));
		}

		private void CreateNullableDoubleTransactionTask(string commandName)
		{
			_transactionTasks.Enqueue(new NullableDoubleTransactionTask(commandName, this.Encoding));
		}

		private void CreateTransactionTask<TData, TResult>(Func<TData, TResult> parse)
		{
			_transactionTasks.Enqueue(new GenericTransactionTask<TData, TResult>(null, parse));
		}

		public void Multi()
		{
			if (_transactionTasks != null)
				throw new InvalidOperationException();
			SendQueryExpectOk("MULTI");
			_transactionTasks = new Queue<ITransactionTask>();
		}

		public void Discard()
		{
			SendQueryExpectOk("DISCARD");
			CleanTransactionTasks();
		}

		private protected void CleanTransactionTasks()
		{
			Volatile.Write(ref _transactionTasks, null);
		}

	}
}