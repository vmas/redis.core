﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using RespService.DataTypes;
using static Redis.Core.Utils;

namespace Redis.Core
{
	public partial class RedisNativeClient
	{
		public virtual long ZAdd(string key, double score, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return SendQueryExpectNumber("ZADD", key, score.ToString(NumberFormatInfo.InvariantInfo), member);
		}

		public virtual long ZAdd(string key, double defaultScore, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (members == null)
				throw new ArgumentNullException(nameof(members));

			if (members.Length == 0)
				return 0L;

			return ZAdd(key, ExistenceMode.Default, false, ValidateKeys(members, nameof(members)).Select(item => new KeyValuePair<string, double>(item, defaultScore)));
		}

		public virtual string ZAdd(string key, ExistenceMode mode, bool returnChanged, bool incr, double score, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (member == null)
				throw new ArgumentNullException(nameof(member));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZADD");
			cmdWithArgs.Add(key);
			switch (mode)
			{
				case ExistenceMode.XX:
					cmdWithArgs.Add("XX");
					break;
				case ExistenceMode.NX:
					cmdWithArgs.Add("NX");
					break;
			}

			if (returnChanged)
			{
				cmdWithArgs.Add("CH");
			}

			if (incr)
			{
				cmdWithArgs.Add("INCR");
			}

			cmdWithArgs.Add(score.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(member);
			return SendQueryExpectString(cmdWithArgs);
		}

		public virtual long ZAdd(string key, ExistenceMode mode, bool returnChanged, IEnumerable<KeyValuePair<string, double>> valuesWithScores)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			if (valuesWithScores == null)
				throw new ArgumentNullException(nameof(valuesWithScores));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZADD");
			cmdWithArgs.Add(key);

			switch (mode)
			{
				case ExistenceMode.XX:
					cmdWithArgs.Add("XX");
					break;
				case ExistenceMode.NX:
					cmdWithArgs.Add("NX");
					break;
			}

			if (returnChanged)
			{
				cmdWithArgs.Add("CH");
			}

			int countWithoutMembers = cmdWithArgs.Count;
			foreach (KeyValuePair<string, double> kv in valuesWithScores)
			{
				cmdWithArgs.Add(kv.Value.ToString(NumberFormatInfo.InvariantInfo));
				if (kv.Key == null)
					throw new ArgumentOutOfRangeException(nameof(valuesWithScores));
				cmdWithArgs.Add(kv.Key);
			}

			if (cmdWithArgs.Count == countWithoutMembers)
				return 0L;

			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long ZCard(string key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return SendQueryExpectNumber("ZCARD", key);
		}

		public virtual long ZCount(string key, double min, double max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			string aMin = double.IsInfinity(min) ? "-inf" : min.ToString(NumberFormatInfo.InvariantInfo);
			string aMax = double.IsInfinity(max) ? "+inf" : max.ToString(NumberFormatInfo.InvariantInfo);
			return SendQueryExpectNumber("ZCOUNT", key, aMin, aMax);
		}


		public virtual long ZCount(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			string aMin, aMax;

			if (double.IsInfinity(min))
				aMin = "-inf";
			else if (excludeMin)
				aMin = "(" + min.ToString(NumberFormatInfo.InvariantInfo);
			else
				aMin = min.ToString(NumberFormatInfo.InvariantInfo);

			if (double.IsInfinity(max))
				aMax = "-inf";
			else if (excludeMax)
				aMax = "(" + max.ToString(NumberFormatInfo.InvariantInfo);
			else
				aMax = max.ToString(NumberFormatInfo.InvariantInfo);

			return SendQueryExpectNumber("ZCOUNT", key, aMin, aMax);
		}

		public virtual double ZIncrBy(string key, double increment, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			return StringToDouble(SendQueryExpectString("ZINCRBY", key, increment.ToString(NumberFormatInfo.InvariantInfo), member));
		}

		public virtual long ZInterStore(string destination, UnionAggregate aggregate, params string[] keys)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			if (keys.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keys));
			var cmdWithArgs = new List<string>(6 + keys.Length);
			cmdWithArgs.Add("ZINTERSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(keys.Length.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.AddRange(ValidateKeys(keys, nameof(keys)));
			if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			else if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long ZInterStore(string destination, IEnumerable<string> keys, UnionAggregate aggregate)
		{
			if (keys == null)
				throw new ArgumentNullException(nameof(keys));
			return ZInterStore(destination, aggregate, keys.ToArray());
		}

		public virtual long ZInterStore(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keysWithWeights == null)
				throw new ArgumentNullException(nameof(keysWithWeights));

			KeyValuePair<string, double>[] kwdata = keysWithWeights.ToArray();
			if (kwdata.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(keysWithWeights));

			var cmdWithArgs = new List<string>(7 + 2 * kwdata.Length);
			cmdWithArgs.Add("ZINTERSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(kwdata.Length.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.AddRange(ValidateKeys(kwdata.Select(kvp => kvp.Key), nameof(keysWithWeights)));
			cmdWithArgs.Add("WEIGHTS");
			cmdWithArgs.AddRange(kwdata.Select(kvp => kvp.Value.ToString(NumberFormatInfo.InvariantInfo)));
			if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			else if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long ZLexCount(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return SendQueryExpectNumber("ZLEXCOUNT", key, min, max);
		}

		public virtual ICollection<string> ZRange(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRangeWithScores(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSortedSetWithScores(SendQueryExpectMultiBulk("ZRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo), "WITHSCORES"));
		}

		public virtual ICollection<string> ZRangeByLex(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZRANGEBYLEX", key, min, max));
		}

		public virtual ICollection<string> ZRangeByLex(string key, string min, string max, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZRANGEBYLEX", key, min, max,
				"LIMIT", offset.ToString(NumberFormatInfo.InvariantInfo), count.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual ICollection<string> ZRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			return CreateSortedSet(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<string> ZRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			return CreateSortedSet(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");

			return CreateSortedSetWithScores(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("ZRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			return CreateSortedSetWithScores(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual long? ZRank(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			RespData response = SendQueryExpectStatement(MakeCommand("ZRANK", key, member));
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64();
			if (response.DataType == RespDataType.Bulk)
				return null;
			throw GetUnexpectedReplyException("ZRANK");
		}

		public virtual long ZRem(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			return SendQueryExpectNumber("ZREM", key, member);
		}

		public virtual long ZRem(string key, string member, params string[] members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			return SendQueryExpectNumber(MakeCommand("ZREM", key, member, ValidateKeys(members, nameof(members))));
		}

		public virtual long ZRem(string key, IEnumerable<string> members)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (members == null)
				throw new ArgumentNullException(nameof(members));
			string[] membersArg = members.ToArray();
			if (membersArg.Length == 0)
				throw new ArgumentOutOfRangeException(nameof(members));
			return SendQueryExpectNumber(MakeCommand("ZREM", key, ValidateKeys(membersArg, nameof(members))));
		}

		public virtual long ZRemRangeByLex(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return SendQueryExpectNumber("ZREMRANGEBYLEX", key, min, max);
		}

		public virtual long ZRemRangeByRank(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			return SendQueryExpectNumber("ZREMRANGEBYRANK", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo));
		}

		public virtual long ZRemRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZREMRANGEBYSCORE");
			cmdWithArgs.Add(key);

			if (double.IsInfinity(min))
				cmdWithArgs.Add("-inf");
			else if (excludeMin)
				cmdWithArgs.Add("(" + min.ToString(NumberFormatInfo.InvariantInfo));
			else
				cmdWithArgs.Add(min.ToString(NumberFormatInfo.InvariantInfo));

			if (double.IsInfinity(max))
				cmdWithArgs.Add("+inf");
			else if (excludeMax)
				cmdWithArgs.Add("(" + max.ToString(NumberFormatInfo.InvariantInfo));
			else
				cmdWithArgs.Add(max.ToString(NumberFormatInfo.InvariantInfo));

			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual ICollection<string> ZRevRange(string key, long start, long stop)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZREVRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRevRangeWithScores(string key, long start, long stop)
		{
			return CreateSortedSetWithScores(SendQueryExpectMultiBulk("ZREVRANGE", key, start.ToString(NumberFormatInfo.InvariantInfo), stop.ToString(NumberFormatInfo.InvariantInfo), "WITHSCORES"));
		}

		public virtual ICollection<string> ZRevRangeByLex(string key, string min, string max)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZREVRANGEBYLEX", key, min, max));
		}

		public virtual ICollection<string> ZRevRangeByLex(string key, string min, string max, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (min == null)
				throw new ArgumentNullException(nameof(min));
			if (max == null)
				throw new ArgumentNullException(nameof(max));
			return CreateSortedSet(SendQueryExpectMultiBulk("ZREVRANGEBYLEX", key, min, max, "LIMIT", offset.ToString(NumberFormatInfo.InvariantInfo), count.ToString(NumberFormatInfo.InvariantInfo)));
		}

		public virtual ICollection<string> ZRevRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(4);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			return CreateSortedSet(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<string> ZRevRangeByScore(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			return CreateSortedSet(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRevRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(5);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");

			return CreateSortedSetWithScores(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual ICollection<KeyValuePair<string, double>> ZRevRangeByScoreWithScores(string key, double min, bool excludeMin, double max, bool excludeMax, long offset, long count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(8);
			cmdWithArgs.Add("ZREVRANGEBYSCORE");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(excludeMin ? "(" + min.ToString(NumberFormatInfo.InvariantInfo) : min.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(excludeMax ? "(" + max.ToString(NumberFormatInfo.InvariantInfo) : max.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add("WITHSCORES");
			cmdWithArgs.Add("LIMIT");
			cmdWithArgs.Add(offset.ToString(NumberFormatInfo.InvariantInfo));
			cmdWithArgs.Add(count.ToString(NumberFormatInfo.InvariantInfo));

			return CreateSortedSetWithScores(SendQueryExpectMultiBulk(cmdWithArgs));
		}

		public virtual long? ZRevRank(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));
			RespData response = SendQueryExpectStatement(MakeCommand("ZREVRANK", key, member));
			if (response.DataType == RespDataType.Integer)
				return ((RespInteger)response).GetInt64();
			if (response.DataType == RespDataType.Bulk)
				return null;
			throw GetUnexpectedReplyException("ZREVRANK");
		}

		public virtual ScanResult<ICollection<KeyValuePair<string, double>>> ZScan(string key, long cursor, string pattern = null, int? count = null)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add(cursor.ToString(NumberFormatInfo.InvariantInfo));

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				if (count.Value <= 0)
					throw new ArgumentOutOfRangeException(nameof(count));

				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
			if (multiData.Length == 2)
			{
				RespData cursorData = multiData[0];
				RespData itemsData = multiData[1];
				if (cursorData.DataType == RespDataType.Bulk
					&& itemsData.DataType == RespDataType.Array
					&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out cursor))
				{
					return new ScanResult<ICollection<KeyValuePair<string, double>>>(cursor, CreateSortedSetWithScores((MultiBulk)itemsData));
				}
			}
			throw GetUnexpectedReplyException("ZSCAN");
		}

		public virtual ScanResult<ICollection<KeyValuePair<string, double>>> ZScan(string key, string pattern = null, int? count = null)
		{
			bool isKey = false;
			var dict = new Dictionary<string, double>();
			foreach (string item in ZScanInternal(key, pattern, count))
			{
				isKey = !isKey;
				if (isKey)
				{
					key = item;
					continue;
				}
				dict[key] = StringToDouble(item);
			}
			return new ScanResult<ICollection<KeyValuePair<string, double>>>(0, dict);
		}

		protected virtual IEnumerable<string> ZScanInternal(string key, string pattern, int? count)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			var cmdWithArgs = new List<string>(7);
			cmdWithArgs.Add("ZSCAN");
			cmdWithArgs.Add(key);
			cmdWithArgs.Add("0");

			if (!string.IsNullOrWhiteSpace(pattern))
			{
				cmdWithArgs.Add("MATCH");
				cmdWithArgs.Add(pattern.Trim());
			}

			if (count != null)
			{
				if (count.Value <= 0)
					throw new ArgumentOutOfRangeException(nameof(count));

				cmdWithArgs.Add("COUNT");
				cmdWithArgs.Add(count.Value.ToString(NumberFormatInfo.InvariantInfo));
			}

			while (true)
			{
				RespData[] multiData = SendQueryExpectArray(cmdWithArgs).ToArray();
				if (multiData.Length == 2)
				{
					RespData cursorData = multiData[0];
					RespData itemsData = multiData[1];
					if (cursorData.DataType == RespDataType.Bulk
						&& itemsData.DataType == RespDataType.Array
						&& long.TryParse(((RespBulk)cursorData).GetString(this.Encoding), out long cursor))
					{
						foreach (RespBulk bulk in ((MultiBulk)itemsData))
						{
							yield return bulk.GetString(this.Encoding);
						}
						if (cursor == 0)
							yield break;
						cmdWithArgs[1] = cursor.ToString(NumberFormatInfo.InvariantInfo);
						continue;
					}
				}
				throw GetUnexpectedReplyException("ZSCAN");
			}
		}

		public virtual double? ZScore(string key, string member)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			string score = SendQueryExpectString("ZSCORE", key, member);
			return score != null ? new double?(StringToDouble(score)) : null;
		}

		public virtual long ZUnionStore(string destination, UnionAggregate aggregate, params string[] keys)
		{
			return ZUnionStore(destination, keys, aggregate);
		}

		public virtual long ZUnionStore(string destination, IEnumerable<string> keys, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));

			if (keys == null)
				throw new ArgumentNullException(nameof(keys));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZUNIONSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(null);
			int count = cmdWithArgs.Count;
			cmdWithArgs.AddRange(ValidateKeys(keys, nameof(keys)));
			count = cmdWithArgs.Count - count;
			if (count > 0)
			{
				cmdWithArgs[2] = count.ToString(NumberFormatInfo.InvariantInfo);
			}
			else
			{
				Expire(destination, 0);
				return 0L;
			}

			if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			else if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}
			return SendQueryExpectNumber(cmdWithArgs);
		}

		public virtual long ZUnionStore(string destination, IEnumerable<KeyValuePair<string, double>> keysWithWeights, UnionAggregate aggregate)
		{
			if (destination == null)
				throw new ArgumentNullException(nameof(destination));
			if (keysWithWeights == null)
				throw new ArgumentNullException(nameof(keysWithWeights));

			var cmdWithArgs = new List<string>();
			cmdWithArgs.Add("ZUNIONSTORE");
			cmdWithArgs.Add(destination);
			cmdWithArgs.Add(null);

			var weights = new List<string>();
			foreach (KeyValuePair<string, double> kvp in keysWithWeights)
			{
				if (kvp.Key == null)
					throw new ArgumentOutOfRangeException(nameof(keysWithWeights));
				cmdWithArgs.Add(kvp.Key);
				weights.Add(kvp.Value.ToString(NumberFormatInfo.InvariantInfo));
			}
			if (weights.Count == 0)
			{
				Expire(destination, 0);
				return 0L;
			}
			else
			{
				cmdWithArgs.Add("WEIGHTS");
				cmdWithArgs[2] = weights.Count.ToString(NumberFormatInfo.InvariantInfo);
				cmdWithArgs.AddRange(weights);
			}

			if (aggregate == UnionAggregate.Max)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MAX");
			}
			else if (aggregate == UnionAggregate.Min)
			{
				cmdWithArgs.Add("AGGREGATE");
				cmdWithArgs.Add("MIN");
			}

			return SendQueryExpectNumber(cmdWithArgs);
		}

	}
}
