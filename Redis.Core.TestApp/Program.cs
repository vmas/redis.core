﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Redis.Core.TestApp
{
	public class Program
	{
		private const string DEFAULT_CONNECTION = "password@127.0.0.1:6379";

		public static void Main(string[] args)
		{
			Console.WriteLine("Select mode:");
			Console.WriteLine("  1 - simple test");
			Console.WriteLine("  2 - benchmark");

			if (int.TryParse(Console.ReadLine().Trim(), out int mode))
			{
				switch (mode)
				{
					case 1:
						RunSimpleTestAsync(args.FirstOrDefault()).Wait();
						break;
					case 2:
						int starvation;
						do
						{
							Console.Write("Select starvation (0% - 99%): ");
						} while (!int.TryParse(Console.ReadLine().Trim(), out starvation) || starvation < 0 || starvation >= 100);
						RunBenchmark(args.FirstOrDefault(), (100 - starvation) / 100.0);
						break;
				}
			} 

			Console.WriteLine("Press any key...");
			Console.ReadKey();
		}

		private static async Task RunSimpleTestAsync(string hostinfo)
		{
			using (var redisPool = new RedisClientsPool("Servers=" + (hostinfo ?? DEFAULT_CONNECTION) + ";Pool Size Multiplier=1"))
			{
				using (var client = await redisPool.GetClientAsync())
				{
					var elemenys = client.Scan().Elements.ToArray();
					await client.DelAsync("hash");
					await client.HMSetAsync("hash", new Dictionary<string, string> { ["name"] = "Jack", ["age"] = "33" });
					ScanResult<IDictionary<string, string>> dict = await client.HScanAsync("hash", 0);
					Console.WriteLine("demo = " + (await client.GetAsync("demo")));
				}
			}
		}



		private static int _benchmarkErrorsCount;
		private static int _benchmarkThreadsCount;
		private static RedisClientsPool RedisPool;

		private static void RunBenchmark(string hostinfo, double multiplier)
		{
			const int THREADS_COUNT = 400;
			RedisPool = new RedisClientsPool("Servers=" + (hostinfo ?? DEFAULT_CONNECTION) + ";Pool Size Multiplier=" + (Math.Max((int)(THREADS_COUNT * multiplier), 1)).ToString());
			RedisPool.PoolTimeout = 2000;

			Interlocked.Exchange(ref _benchmarkErrorsCount, 0);
			Interlocked.Exchange(ref _benchmarkThreadsCount, 0);
			
			using (var ev = new ManualResetEvent(false))
			{
				for (int i = 0; i < THREADS_COUNT; i++)
				{
					var thread = new Thread(BenchmarkWork);
					thread.IsBackground = true;
					thread.Start(ev);
				}

				while (true)
				{
					Thread.Sleep(100);
					Console.WriteLine("PoolInfo: {0}; errors: {1}", RedisPool.Performance, Volatile.Read(ref _benchmarkErrorsCount));
					if (Volatile.Read(ref _benchmarkThreadsCount) == THREADS_COUNT)
					{
						ev.Set();
					}
				}
			}
		}

		private static void BenchmarkWork(object startObj)
		{
			byte[] buffer = new byte[1];
			using (var random = RandomNumberGenerator.Create())
				random.GetBytes(buffer);

			int sleepTime = buffer[0];
			sleepTime = sleepTime * new Random(sleepTime).Next(1, 11);

			Interlocked.Increment(ref _benchmarkThreadsCount);
			var ev = (ManualResetEvent)startObj;
			ev.WaitOne();

			Thread.Sleep(sleepTime);

			while (true)
			{
				try
				{
					BenchmarkWorkAsync().Wait();
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					Environment.Exit(-1);
				}
				Thread.Sleep(1);
			}
		}

		private static async Task<int> BenchmarkWorkAsync()
		{
			for (int i = 0; i < 6; i++)
			{
				try
				{
					using (var client = await RedisPool.GetClientAsync())
					{
						await client.IncrAsync("demo");
					}
				}
				catch (Exception ex)
				{
					Interlocked.Increment(ref _benchmarkErrorsCount);
				}
			}
			return 2;
		}

	}
}
